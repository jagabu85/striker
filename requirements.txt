!! WARNING !!
If you do not know what Oofelie is stop reading and forget about this module
It is not usefull at all to you.

Requirements:
	numpy
	pyLatex
	matplotlib
	math
	os

	Sphinx
	phinx_rtd_theme
	sphinxcontrib-tikz

Description:
It helps to create script files to use the simulation software Oofelie.
It was developed for the concret case of the striker simulation, despite it could be reused for other cases involving shafts.

Class shaft:
It helps to create the node mesh and elements in the Oofelie format to simulate mechanical shafts. The shaft is discretized with beam elements, only for 2D simulation. 
Massless rigid bodies are created to describe the outer geometry of the shaft to account the possibility to have contact with other surfaces.

Class Simulation:
Allos to account the number of nodes and elements used in the script.
