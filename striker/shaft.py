import matplotlib as mpl
import matplotlib.pyplot as plt

from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from matplotlib import cm, colors
from matplotlib import gridspec

from . import *
from .section import Section
from .utils import *

import sys

import matplotlib.animation as animation
import matplotlib.patches as patches

#import matplotlib.cm as cm
#import matplotlib.mlab as mlab
#from matplotlib.path import Path
#from matplotlib.patches import PathPatch


class Shaft:
    """ Contains the geometrical information needed to discretize a shaft \
    with beam elements and massless rigid elements to account for the \
    contacts, ass seen in the :ref:`intro-label`.

    .. tikz::

        %upper shaft
        \draw[line width=0.4mm, dashed] (-0.5,0)--(5,0);

        \draw[line width=0.6mm](0,0)--(0,0.5)--(1,0.5)--
        (2,1)--(3.5,1)--(3.5,0.5)--(4.5,0.5)--(4.5,0);

        \draw[line width=0.6mm](0,0)--(0,-0.5)--(1,-0.5)--
        (2,-1)--(3.5,-1)--(3.5,-0.5)--(4.5,-0.5)--(4.5,0);

        \draw[line width=0.6mm] (1,0.5)--(1,-0.5);
        \draw[line width=0.6mm] (2,1)--(2,-1);
        \draw[line width=0.6mm] (3.5,1)--(3.5,-1);

        \draw (0.5,0)node[color=red, anchor=center] {1};
        \draw (1.5,0)node[color=red, anchor=center] {2};
        \draw (2.75,0)node[color=red, anchor=center] {3};
        \draw (4,0)node[color=red, anchor=center] {4};

        %lower discretization
        \draw[line width=0.4mm, dashed] (-0.5,-3)--(5,-3);

        % Beam nodes
        \draw[line width=0.4mm] (0,-3)--(4.5,-3);
        \draw[fill=black] (0,  -3) circle (0.07cm);
        \draw[fill=black] (0.5,-3) circle (0.07cm);
        \draw[fill=black] (1,  -3) circle (0.07cm);
        \draw[fill=black] (1.5,-3) circle (0.07cm);
        \draw[fill=black] (2,  -3) circle (0.07cm);
        \draw[fill=black] (2.5,-3) circle (0.07cm);
        \draw[fill=black] (3,  -3) circle (0.07cm);
        \draw[fill=black] (3.5,-3) circle (0.07cm);
        \draw[fill=black] (4,  -3) circle (0.07cm);
        \draw[fill=black] (4.5,-3) circle (0.07cm);

        %Upper nodes
        \draw[fill=red, color = red] (0,-2.5) circle (0.07cm);
        \draw[fill=red, color = red] (0.5,-2.5) circle (0.07cm);
        \draw[fill=red, color = red] (1,-2.5) circle (0.07cm);
        \draw[fill=red, color = red] (1.5,-2.25) circle (0.07cm);
        \draw[fill=red, color = red] (2,-2) circle (0.07cm);
        \draw[fill=red, color = red] (2.5,-2) circle (0.07cm);
        \draw[fill=red, color = red] (3,-2) circle (0.07cm);
        \draw[fill=red, color = red] (3.5,-2) circle (0.07cm);
        \draw[fill=red, color = red] (4,-2.5) circle (0.07cm);
        \draw[fill=red, color = red] (4.5,-2.5) circle (0.07cm);

        %Lower nodes
        \draw[fill=red, color = red] (0,  -3.5) circle (0.07cm);
        \draw[fill=red, color = red] (0.5,-3.5) circle (0.07cm);
        \draw[fill=red, color = red] (1,  -3.5) circle (0.07cm);
        \draw[fill=red, color = red] (1.5,-3.75) circle (0.07cm);
        \draw[fill=red, color = red] (2,  -4) circle (0.07cm);
        \draw[fill=red, color = red] (2.5,-4) circle (0.07cm);
        \draw[fill=red, color = red] (3,  -4) circle (0.07cm);
        \draw[fill=red, color = red] (3.5,-4) circle (0.07cm);
        \draw[fill=red, color = red] (4,  -3.5) circle (0.07cm);
        \draw[fill=red, color = red] (4.5,-3.5) circle (0.07cm);

        %Rigid element lines
        \draw[line width=0.4mm, color = red] (0,-2.5)--(0,-3.5);
        \draw[line width=0.4mm, color = red] (0.5,-2.5)--(0.5,-3.5);
        \draw[line width=0.4mm, color = red] (1,-2.5)--(1,-3.5);
        \draw[line width=0.4mm, color = red] (1.5,-2.25)--(1.5,-3.75);
        \draw[line width=0.4mm, color = red] (2,-2)--(2,-4);
        \draw[line width=0.4mm, color = red] (2.5,-2)--(2.5,-4);
        \draw[line width=0.4mm, color = red] (3,-2)--(3,-4);
        \draw[line width=0.4mm, color = red] (3.5,-2)--(3.5,-4);
        \draw[line width=0.4mm, color = red] (4,-2.5)--(4,-3.5);
        \draw[line width=0.4mm, color = red] (4.5,-2.5)--(4.5,-3.5);

        %Contact
        \draw[line width=0.8mm] (1.75,-1.8)--(3.75,-1.8);
        \draw[fill=red, color = black] (2.75,-1.8) circle (0.1cm);

        \draw[line width=0.8mm] (1.75,-4.2)--(3.75,-4.2);
        \draw[fill=red, color = black] (2.75,-4.2) circle (0.1cm);

    :param section_list: Array of containing all the geometrical info of the \
    shaft. Ex: section_info = np.array([[0.5, 0.5, 1, 2], \
                                        [0.5, 1, 2, 2, None],\
                                        [1, 1, 1.5, 3, 1.2],\
                                        [0.5, 0.5, 1, 2, None]])
    :type section_list: np.array, mandatory
    :param num_elements: Total number of beam elements in the shaft
    :type num_elements: double, mandatory
    :param num_nodes_beam: Total number of nodes used to define the beam \
    elements
    :type num_nodes_beam: double, mandatory
    :param nodes: Array storing the coordinates of all nodes used to \
    discretize the shaft, the nodes of the beam elements + the nodes of the \
    massless rigid bodies.
    :type nodes: numpy array, mandatory
    :param contact_nodes_up: Array with the coordinates of the nodes used to \
    define the contacts of the upper part
    :type contact_nodes_up: numpy array, mandatory
    :param contact_nodes_down: Array with the coordinates of the nodes used to \
    define the contacts of the lower part
    :type contact_nodes_down: numpy array, mandatory
    :param nodal_volume: Volume belonging to each node. It will \
    be used to compute the gravity load.
    :type nodal_volume: numpy array, mandatory
    :param last_used_node: Counter, store the number of nodes already used in \
    the shaft
    :type last_used_node: int, mandatory
    :param last_used_element: Counter, store the number of nodes already used \
    in the shaft
    :type last_used_element: int, mandatory
    :param id_nodes: Dictionary used to give an understandable label to each \
    important node. Ex:
    :type id_nodes: dictionary, optional
    """

    def __init__(self, section_info):
        """Constructor method

        :param section_list: Array of Section objects
        :type section_list: np.array, mandatory
        """

        self.section_list = []
        for section_parameters in section_info:
            self.section_list.append(Section(section_parameters[0],
                                             section_parameters[1],
                                             section_parameters[2],
                                             int(section_parameters[3]),
                                             section_parameters[4]))
        self.num_elements = self.compute_num_elements()
        self.num_nodes_beam = self.num_elements + 1  # num beam nodes
        self.set_id_nodes()

        self.nodes = self.create_shaft_nodes()
        self.contact_nodes_up, self.contact_nodes_down = \
            self.create_contact_nodes()
        self.nodal_volume = self.compute_nodal_volume()

        self.last_used_node = 0
        self.last_used_element = 0
        self.id_nodes = {}

    def set_id_nodes(self):
        """For each one of the sections, this fuction set the starting and \
        ending nodes for each one of the sections of the shaft. More info \
        check the class Section.
        """
        i = 0
        for section in self.section_list:
            section.id_ini = i
            section.id_end = i + section.num_elements
            section.id_ini_up = i + self.num_nodes_beam
            section.id_end_up = i + section.num_elements + self.num_nodes_beam
            section.id_ini_down = i + self.num_nodes_beam * 2
            section.id_end_down = i + section.num_elements + \
                                  self.num_nodes_beam * 2
            i += section.num_elements

    def get_num_nodes_beam(self):
        """
        :return: Total number of nodes used to define the beam elements
        :rtype: int
        """
        return self.num_nodes_beam

    def get_last_used_node(self):
        """
        :return: Return the status of the counter self.last_used_node
        :rtype: int
        """
        return self.last_used_node

    def get_next_used_node(self, dict_key=None):
        """ Add 1 to the counter self.last_used_node,
        Safe the value of self.last_used_node in the dictionary self.id_nodes

        :param dict_key: label for the node
        :type dict_key: string
        :return: Return the status of the counter self.last_used_element
        :rtype: int
        """
        self.last_used_node = self.last_used_node + 1
        if dict_key != None:
            self.id_nodes[dict_key] = self.last_used_node
        return self.last_used_node

    def get_next_used_element(self):
        """ Add 1 to the counter self.last_used_element

        :return: Return the status of the counter self.last_used_element
        :rtype: int
        """
        self.last_used_element = self.last_used_element + 1
        return self.last_used_element

    def compute_num_elements(self):
        """ Compute the total number of beam elements in the shaft. It sum the \
        number of beam elements in each section.

        :return: total number of elements
        :rtype: int
        """
        total_num_elements = 0
        for section in self.section_list:
            total_num_elements = total_num_elements + section.num_elements
        return total_num_elements

    def compute_x_coordinates(self):
        """ Compute the position in x for each one of the beam nodes

        :return: vector storing the coordinates in X for the beam nodes
        :rtype: numpy array
        """
        coordinates = np.zeros((self.num_nodes_beam))
        section_initial_position = 0
        for section in self.section_list:
            coordinates[section.id_ini:(section.id_end + 1)] = \
                section.compute_relative_positions() + section_initial_position
            section_initial_position = coordinates[section.id_ini \
                                                   + section.num_elements]
        return coordinates

    def create_shaft_nodes(self):
        """ Creates a matrix with the nodes coordinates for the nodes \
        belonging to the section_list.

        :return: vector storing the coordinates for all the nodes used to \
        describe the beam elements and rigid bodies
        :rtype: np.array [num_nodes_beam*3, 3]
        """
        nodes = np.zeros((self.num_nodes_beam * 3, 3))

        # set positions in x
        nodes[0:self.num_nodes_beam, 0] = \
            self.compute_x_coordinates()
        nodes[self.num_nodes_beam: self.num_nodes_beam * 2, 0] = \
            nodes[0:self.num_nodes_beam, 0]
        nodes[self.num_nodes_beam * 2: self.num_nodes_beam * 3, 0] = \
            nodes[0:self.num_nodes_beam, 0]

        # set positions in y
        idOffset = self.num_nodes_beam
        previous_radius = 0
        for section in self.section_list:
            if section.radius_ini == section.radius_end:
                # print ("-------------------------------------EQUAL")
                if previous_radius <= section.radius_ini:
                    nodes[idOffset + section.id_ini: idOffset + section.id_end
                                                     + 1,
                    1] = section.radius_ini
                    nodes[idOffset * 2 + section.id_ini: \
                          idOffset * 2 + section.id_end + 1,
                    1] = -section.radius_ini
                else:
                    nodes[idOffset + section.id_ini + 1: idOffset +
                                                         section.id_end + 1,
                    1] = section.radius_ini
                    nodes[idOffset * 2 + section.id_ini + 1: \
                          idOffset * 2 + section.id_end + 1,
                    1] = -section.radius_ini
                previous_radius = section.radius_end
            else:
                # print ("-------------------------------------DIFFERENT")
                if previous_radius <= section.radius_ini:
                    delta_y = (section.radius_end - section.radius_ini) / \
                              section.num_elements
                    for i, j in enumerate(
                            range(section.id_ini, section.id_end + 1)):
                        # i: starts at 0; j: starts at section.id_ini
                        nodes[idOffset + j, 1] = section.radius_ini + delta_y \
                                                 * (i)
                        nodes[idOffset * 2 + j, 1] = -(
                                section.radius_ini + delta_y * (i))
                else:
                    for i, j in enumerate(
                            range(section.id_ini + 1, section.id_end + 1)):
                        nodes[idOffset + j, 1] = section.radius_ini + delta_y \
                                                 * (i)
                        nodes[idOffset * 2 + j, 1] = -(
                                section.radius_ini + delta_y * (i))
                previous_radius = section.radius_end
        return nodes

    def apply_deformation_y_to_nodes(self, deformation_neutral, deformation_up,
                                     deformation_down):
        """ Apply a deformation in the Y direction to all nodes in self.nodes \
        (the beam nodes and to the rigid nodes). It is used to apply initial \
        deformation due to manufacturing errors. \
        It modifies self.nodes\
        The size of deformation_neutral, deformation_up and \
        deformation_down are the same and should be the number of nodes \
        used to discretize the beam elements (self.num_nodes_beam)

        :param deformation_neutral: Deformation applied to the nodes of the \
        beam
        :type deformation_neutral: numpy array, mandatory
        :param deformation_up: Deformation applied to the nodes of the \
        rigid body (only the upper part)
        :type deformation_up: numpy array, mandatory
        :param deformation_down: Deformation applied to the nodes of the \
        rigid body (only the lower part)
        :type deformation_down: numpy array, mandatory
        """
        self.nodes[:, 1] += np.concatenate((deformation_neutral,
                                            deformation_up,
                                            deformation_down), axis=0)
        return

    def compute_initial_rotation_angle(self):
        point_a = self.nodes[self.num_nodes_beam*2 + \
                             self.section_list[6].id_end, 0:2]
        point_b = self.nodes[self.num_nodes_beam + \
                             self.section_list[2].id_ini, 0:2]

        point_b[1] += abs(point_a[1])
        point_a[1] += abs(point_a[1])
        point_b[0] += abs(point_a[0])
        point_a[0] += abs(point_a[0])

        alpha = np.arctan(point_b[1]/point_b[0])
        hypotenuse =  np.linalg.norm(point_b)

        c2 = self.section_list[2].radius_contact * 2
        c1 = np.sqrt(hypotenuse**2 - c2**2)
        tetha = np.arctan(c2/c1)
        angle = tetha - alpha

        return angle

    def compute_initial_rotation_matrix(self):
        angle = -self.compute_initial_rotation_angle()
        rotation_matrix = np.array([[np.cos(angle), -np.sin(angle)],
                                     [np.sin(angle), np.cos(angle)]])
        return rotation_matrix

    def apply_initial_rotation(self):
        point_a = self.nodes[self.num_nodes_beam*2 + \
                             self.section_list[6].id_end, 0:2].copy()
        tmp_nodes = self.nodes[:, :]
        tmp_nodes[:,:] -= [point_a[0], point_a[1], 0]
        self.nodes[:,0:2] = self.nodes[:,0:2] @ \
                            self.compute_initial_rotation_matrix()
        self.nodes[:, :] += [point_a[0], point_a[1], 0]

    def move_nodes_y(self, offset):
        """ Move all the strike nodes (beams + rigid) in the Y direction. \
        Useful to move the initital position of the shaft. \
        It modifies self.nodes

        :param offset: Offset applied to all the nodes to describe the shaft
        :type offset: numpy array, mandatory
        """

        offset_y = np.ones([self.num_nodes_beam,1])[:,0] * offset
        self.apply_deformation_y_to_nodes(offset_y, offset_y, offset_y)
        print(f'Offset_y of {offset}mm has been applied')
        return

    def apply_initial_deformation(self):
        """ IT IS NOT USED ! \
        Apply an intitial deformation to all the strike nodes (beams + \
        rigid) in the Y direction. \
        It modifies self.nodes
        """
        x = np.linspace(0, self.nodes[-1, 0], self.num_nodes_beam)
        deformation = x * x * 0.01
        self.apply_deformation_y_to_nodes(deformation, deformation,
                                          deformation)
        print(f'Initial deformation has been applied')
        plt.plot(x, deformation)
        plt.show()
        return

    def compute_volume(self):
        """ Computes the total volume of the shaft

        :return volume: volume of the shaft
        :type volume: double
        """
        volume = 0
        for section in self.section_list:
            volume += section.compute_volume()
        return volume

    def create_contact_nodes(self):
        """ Computes the total volume of the shaft

        :return contact_nodes_up: vector storing the coordinates for the \
        upper nodes used to describe the contacts
        :type contact_nodes_up: numpy array
        :return contact_nodes_down:  vector storing the coordinates for the \
        lower nodes used to describe the contacts
        :type contact_nodes_down: numpy array
        """
        contact_nodes_up = []
        contact_nodes_down = []

        for i, section in enumerate(self.section_list):
            if section.radius_contact != None:
                posX = (self.nodes[section.id_ini, 0] + \
                        self.nodes[section.id_end, 0]) / 2
                contact_nodes_up.append([i, posX, section.radius_contact, 0])
                contact_nodes_down.append([i, posX, -section.radius_contact,
                                           0])
        contact_nodes_up = np.array(contact_nodes_up)
        contact_nodes_down = np.array(contact_nodes_down)
        return contact_nodes_up, contact_nodes_down

    def compute_nodal_volume(self):
        """ Computes the volume of influence for each node, it is used to
        compute the gravity load.

        :return nodal_volume: volume of influence for each node
        :type nodal_volume: numpy array
        """
        nodal_volume = np.zeros((self.num_nodes_beam))

        counter = 0
        for i, section in enumerate(self.section_list):
            delta_y = ((section.radius_end - section.radius_ini)
                       / section.num_elements)
            delta_x = section.length / section.num_elements
            for j in range(1, section.num_elements + 1):
                radius_center_element = (section.radius_ini - delta_y * 0.5 +
                                         delta_y * j)
                nodal_volume[counter] += abs(compute_circle_area(
                    radius_center_element) * delta_x * 0.5)
                nodal_volume[counter + 1] += abs(compute_circle_area(
                    radius_center_element) * delta_x * 0.5)
                counter += 1
        return nodal_volume

    def create_string_elements_beam_definition(self):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the beam elements (it defines the propelem, it \
        means it creates the object of Line2BeamNL for each one of the \
        different sections.

        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ""
        for i, section in enumerate(self.section_list):
            if section.radius_ini == section.radius_end:
                # print("EQUAL")
                tmp += f'''Propelem eleBeam_section_{i + 1} (Line2BeamNL_new_E);
eleBeam_section_{i + 1}.put (DIRECTION_2, 0,0,1);
eleBeam_section_{i + 1}.put (CROSS_SECTION_1, {compute_circle_area(section.radius_ini):.4e});
eleBeam_section_{i + 1}.put (INERTIA_1,   {compute_inertia_circle(section.radius_ini):.4e});
eleBeam_section_{i + 1}.put (INERTIA_2,   {compute_inertia_circle(section.radius_ini):.4e});
eleBeam_section_{i + 1}.put (INERTIA_3,   {compute_polar_inertia_circle(section.radius_ini):.4e});
eleBeam_section_{i + 1}.put (WITH_RESIDUAL_BENDING_FLEXIBILITY_CORRECTION, 1);
eleBeam_section_{i + 1}.put (MATERIAL,   1);
dom.add(eleBeam_section_{i + 1});
//-----------------------------------------------------------------
'''
            else:
                delta_y = (section.radius_end - \
                           section.radius_ini) / section.num_elements
                for j in range(1, section.num_elements + 1):
                    radius_center_element = section.radius_ini - delta_y * 0.5 \
                                            + delta_y * j
                    tmp += f'''Propelem eleBeam_section_{i + 1}_{j} (Line2BeamNL_new_E);
eleBeam_section_{i + 1}_{j}.put (DIRECTION_2, 0,0,1);
eleBeam_section_{i + 1}_{j}.put (CROSS_SECTION_1, {compute_circle_area(radius_center_element):.4e});
eleBeam_section_{i + 1}_{j}.put (INERTIA_1,   {compute_inertia_circle(radius_center_element):.4e});
eleBeam_section_{i + 1}_{j}.put (INERTIA_2,   {compute_inertia_circle(radius_center_element):.4e});
eleBeam_section_{i + 1}_{j}.put (INERTIA_3,   {compute_polar_inertia_circle(radius_center_element):.4e});
eleBeam_section_{i + 1}_{j}.put (WITH_RESIDUAL_BENDING_FLEXIBILITY_CORRECTION, 1);
eleBeam_section_{i + 1}_{j}.put (MATERIAL,   1);
dom.add(eleBeam_section_{i + 1}_{j});
//-----------------------------------------------------------------
'''
                    # print("DIFFERENT")
        return tmp[:-1]

    def create_string_beam_nodes(self, _simulation):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the beam nodes

        :param _simulation: Object simulation, contain the last used IDs in \
        oofelie
        :type _simulation: object simulation,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ""
        for node in self.nodes:
            tmp += f'posNodes.define({self.get_next_used_node()}, ' \
                   f'{node[0]:.7f}, {node[1]:.7f}, {node[2]:.7f});\n'
        _simulation.last_oofelie_node = self.last_used_node
        return tmp[:-1]

    def create_string_contact_nodes_up(self, _simulation):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the contact nodes (upper part)

        :param _simulation: Object simulation, contain the last used IDs in \
        oofelie
        :type _simulation: object simulation,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ''
        for i, node in enumerate(self.contact_nodes_up):
            self.section_list[int(node[0])].id_contact_up = \
                self.get_next_used_node()
            tmp += f'posNodes.define({self.last_used_node}, ' \
                   f'{node[1]:.7f}, {node[2]:.7f}, {node[3]:.7f});' \
                   f'// Contact node for section: {int(node[0])}\n'
        _simulation.last_oofelie_node = self.last_used_node
        return tmp[:-1]

    def create_string_contact_nodes_down(self, _simulation):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the contact nodes (lower)

        :param _simulation: Object simulation, contain the last used IDs in \
        oofelie
        :type _simulation: object simulation,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ''
        for i, node in enumerate(self.contact_nodes_down):
            self.section_list[int(node[0])].id_contact_down = \
                self.get_next_used_node()
            tmp += f'posNodes.define({self.last_used_node}, ' \
                   f'{node[1]:.7f}, {node[2]:.7f}, {node[3]:.7f});' \
                   f'// Contact node for section: {int(node[0])}\n'
        _simulation.last_oofelie_node = self.last_used_node
        return tmp[:-1]

    def create_string_beam_elements_striker(self, _simulation):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the beam elements

        :param _simulation: Object simulation, contain the last used IDs in \
        oofelie
        :type _simulation: object simulation,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ""
        for j, section in enumerate(self.section_list):
            if section.radius_ini == section.radius_end:
                for id_node in range(section.id_ini, section.id_end):
                    tmp += f'elements.define(' \
                           f'{self.get_next_used_element()}, ' \
                           f'eleBeam_section_{j + 1}, {id_node + 1}, ' \
                           f'{id_node + 2});\n'
            else:
                for k, id_node in enumerate(range(section.id_ini,
                                                  section.id_end)):
                    tmp += f'elements.define(' \
                           f'{self.get_next_used_element()}, ' \
                           f'eleBeam_section_{j + 1}_{k + 1}, {id_node + 1},' \
                           f' {id_node + 2});\n'
        _simulation.last_oofelie_element = self.last_used_element
        return tmp[:-1]

    def create_string_rigid_elements_striker(self, _simulation):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the rigid elements

        :param _simulation: Object simulation, contain the last used IDs in \
        oofelie
        :type _simulation: object simulation,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ""
        for i in range(1, self.num_nodes_beam + 1):
            tmp += f'elements.define({self.get_next_used_element()}, ' \
                   f'rigidBodyMassLess, {i}, {self.num_nodes_beam + i}, ' \
                   f'{self.num_nodes_beam * 2 + i});\n'
        _simulation.last_oofelie_element = self.last_used_element
        return tmp[:-1]

    def create_string_contact_elements_up(self, _simulation):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the contact elements (Upper part)

        WARNING !!! FOR TWO CONSECUTIVE SECTIONS WITH CONTACT IT DOES NOT WORK

        :param _simulation: Object simulation, contain the last used IDs in \
        oofelie
        :type _simulation: object simulation,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        # contact_nodes_coordinates : [id_section, x, y, z]
        tmp = ""
        for section in self.section_list:
            if section.radius_ini == section.radius_end and \
                    section.radius_contact != None:
                for i in range(0, section.num_elements + 1):
                    tmp += f'elements.define(' \
                        f'{self.get_next_used_element()},' \
                        f' eleContactCeiling, {section.id_contact_up}, ' \
                        f'{self.num_nodes_beam + section.id_ini + 1 + i});\n'
        _simulation.last_oofelie_element = self.last_used_element
        return tmp[:-1]

    def create_string_contact_elements_down(self, _simulation):
        """ Creates a string to be added in the oofelie script. It \
        is the definition of the contact elements (lower part)

        WARNING !!! FOR TWO CONSECUTIVE SECTIONS WITH CONTACT IT DOES NOT WORK

        :param _simulation: Object simulation, contain the last used IDs in \
        oofelie
        :type _simulation: object simulation,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        # contact_nodes_coordinates : [id_section, x, y, z]
        tmp = ""
        for section in self.section_list:
            if section.radius_ini == section.radius_end and \
                    section.radius_contact != None:
                for i in range(0, section.num_elements + 1):
                    tmp += f'elements.define(' \
                        f'{self.get_next_used_element()}, ' \
                        f'eleContactFloor, {section.id_contact_down}, ' \
                        f'{self.num_nodes_beam * 2 + section.id_ini + 1 + i});\n'
        _simulation.last_oofelie_element = self.last_used_element
        return tmp[:-1]

    def create_string_fix_beam_nodes(self):
        """ Creates a string to be added in the oofelie script. It fix the \
        translations in Z, and the rotations in X and Y

        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ""
        for i, node in enumerate(self.nodes):
            tmp += create_string_to_partial_fix_node(i + 1, ["TZ", "RX", "RY"])
        return tmp[:-1]

    def create_string_fix_contact_nodes(self):
        """ Creates a string to be added in the oofelie script. It fix the \
        contact nodes in all the directions, translation and rotation

        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ""
        for i in (self.contact_nodes_up[:, 0]):
            j = int(i)
            tmp += create_string_to_fix_node(self.section_list[j]. \
                                             id_contact_up,
                                             'upper contact elememts')
        for i in (self.contact_nodes_down[:, 0]):
            j = int(i)
            tmp += create_string_to_fix_node(self.section_list[j]. \
                                             id_contact_down,
                                             'lower contact elememts')
        return tmp[:-1]

    def create_string_gravity_load(self, density, shooting_angle):
        """ Creates a string to be added in the oofelie script. It add a \
        force in the  -Y direction to represent the gravity load

        :param density: density of the shaft material
        :type density: double,  mandatory
        :return tmp: oofelie script
        :type tmp: string
        """
        tmp = ""
        nodal_volume = self.compute_nodal_volume()
        print('Total gravity force on the striker:',
              nodal_volume.sum() * density * 9.81, 'N')
        for i, volume in enumerate(nodal_volume):
            tmp += f'excSet.define({i + 1}, TX|GF, ' \
                   f'{-volume * density * -np.cos(shooting_angle) * 9.81:.4e});\n'
            tmp += f'excSet.define({i + 1}, TY|GF, ' \
                   f'{-volume * density * -np.sin(shooting_angle)* 9.81:.4e});\n'
        return tmp[:-1]

    def get_plot_coordinates_x(self, field_type):
        """ It returns a vector with the coordinates in X used to make plots. \
        To plot results in the nodes, it returns the coordinates of the beam \
        nodes in the X direction. To plot the element variables (like stress) \
        it returns a vector whit the positions between the beam nodes.

        Size of coordinates_x for nodes variables = number of nodes

        Size of coordinates_x for element variables = number of nodes -1

        :param field_type: it can be 'node' or 'element' to know which of \
        coordinates are needed
        :type field_type: string
        :return coordinates_x: vector with the position in X direction to \
        make plots
        :type coordinates_x: numpy array
        """
        if field_type == 'element':
            coordinates_x = self.nodes[1:self.num_nodes_beam, 0]+0.00025
        elif field_type == 'node':
            coordinates_x = self.nodes[0:self.num_nodes_beam, 0]
        else:
            raise ValueError("The argument type in function "
                             "read_results is incorrect, it "
                             "should be: \"element\" or \"node\"")
        return coordinates_x

    def get_all_average_radius(self):
        """ It returns a vector with size = number of sections. This vector \
        stores the average radius for each section

        :return radius: oofelie script
        :type radius: numpy array
        """
        radius = np.zeros(self.num_elements)
        for i in range(0, self.num_elements):
            radius[i] = self.nodes[self.num_nodes_beam + i, 1] + \
                        self.nodes[self.num_nodes_beam + i + 1, 1] * 0.5
        return radius

    def compute_stress(self, strain, field_to_read, variables):
        """ Compute the stress in MPa .
        Note: Negative strain means negative bending, deformation shape _--_
              Positive strain means positive bending, deformation shape -_-

        :param strain: array size[number_time_steps, number_of_beam_elements] \
        with the values of the strains for each one of the time steps.
        :type strain: numpy array
        :param field_to_read: it can be: gamma_1 (axial), gamma_2(shear z), \
        gamma_3 (shear y), kappa_1(torsion), kappa_2 (bending y) or kappa_3 \
        (bending z)
        :type field_to_read: string
        :param variables: dictionary containing all the variables to define \
        the simulation, defined in define_model.py/define_variables()
        :type variables: dictionary
        :return stress: vector with the stress in each one of the sections
        :type stress: numpy array
        """
        # Pa to MPa 1e-6
        if field_to_read == 'gamma_1':
            stress = strain * variables['young_modulus'] * 1e-6
        elif field_to_read == 'gamma_2':
            stress = strain * variables['shear_modulus'] * 1e-6
        elif field_to_read == 'gamma_3':
            stress = strain * variables['shear_modulus'] * 1e-6
        elif field_to_read == 'kappa_1':
            radius = self.get_all_average_radius()
            stress = strain * variables['shear_modulus'] * radius * 1e-6
        elif field_to_read == 'kappa_2':
            # stress is the stress in the upper part of the beam
            # stress(y) = -E * strain * y
            radius = self.get_all_average_radius()
            stress = -strain * variables['young_modulus'] * radius * 1e-6
        elif field_to_read == 'kappa_3':
            radius = self.get_all_average_radius()
            # Computes the strain in the upper part of the beam
            stress = strain * variables['young_modulus'] * radius * 1e-6
        else:
            raise ValueError("The argument field_to_read in function "
                             "compute_stress is incorrect, it should be:  "
                             "gamma_1, gamma_2, gamma_3, kappa_1, "
                             "kappa_2 or kappa_3")

        return stress

    def create_plots_field(self, field_to_read, variables, field_type,
                           units, span = None ):
        """ Plot the field for all the times in a colour scale, also plot \
        the the maximum and minimum value along time for each element. \
        The resulting plots are stored in the folder image.

        :param field_to_read: the name of the file to read and plot
        :type field_to_read: string
        :param variables: dictionary containing all the variables to define \
        the simulation, defined in define_model.py/define_variables()
        :type variables: dictionary
        :param field_type: to identify if the values of the field are saved \
        in the node or in the element. It can only be 'element' or 'node'
        :type field_type: string, mandatory
        :param units: unit to be added in the plot
        :type units: string
        """

        field = read_results(variables['path'], field_to_read, field_type)
        coordinates_x = self.get_plot_coordinates_x(field_type)
        time_discretization = np.linspace(0, variables['final_time'],
                                          field.shape[0], dtype=float)

        #if type(span) == type(None):
        if span is None:
            span = np.array([1, np.shape(field)[0]])
            file_image_name = 'tmp_' + field_to_read
        else:
            file_image_name = 'tmp_' + field_to_read + '_span'

        field = field[span[0]:span[1]]
        time_discretization = time_discretization[span[0]:span[1]]

        if field_type == 'element':
            field = self.compute_stress(field, field_to_read, variables)

        self.plot_field(field, coordinates_x, time_discretization,
                        variables['path'], file_image_name, units)
        self.plot_min_max_field(field, coordinates_x, variables['path'], \
                                file_image_name + '_min_max', units)

    def compute_axial_bending_stress(self, variables):
        """ Comput the max and min stress combining the axial and the bending \
        Note: Negative strain means negative bending, deformation _--_
              Positive strain means positive bending, deformation -_-
        stress:
            upper part of the beam = gamma_1 (axial) + kappa_3 (bending z)
            lower part of the beam = gamma_1 (axial) - kappa_3 (bending z)

        :param variables: dictionary containing all the variables to define \
        the simulation, defined in define_model.py/define_variables()
        :type variables: dictionary
        :return up_beam_stress: axial + bending stress in the upper part of \
        the beam. In MPA
        :type up_beam_stress: numpy array
        """
        axial_strain = read_results(variables['path'], 'gamma_1', 'element')
        bending_strain = read_results(variables['path'], 'kappa_2', 'element')

        axial_stress = self.compute_stress(axial_strain, 'gamma_1', \
                                           variables)
        # this bending_stress is the upper part of the beam
        bending_stress = self.compute_stress(bending_strain, 'kappa_3', \
                                             variables)

        up_beam_stress = axial_stress + bending_stress
        down_beam_stress = axial_stress - bending_stress

        return up_beam_stress, down_beam_stress

    def plot_axial_bending_stress_field(self, variables):
        """ Plot the max and min stress combining the axial and the bending

        :param variables: dictionary containing all the variables to define \
        the simulation, defined in define_model.py/define_variables()
        :type variables: dictionary
        """
        fields_to_plot = [('tmp_axial_bending_up'),
                          ('tmp_axial_bending_down'),
                          ('tmp_axial_bending_up_min_max'),
                          ('tmp_axial_bending_down_min_max')]

        captions_to_plot = [('Stress field (axial + bending) in the upper \
                             part of the striker'),
                            ('Stress field (axial + bending) in the lower \
                            part of the striker'),
                            ('Minimum and maximum stress (axial + bending) \
                            in the upper part of the striker'),
                            ('Minimum and maximum stress (axial + bending) \
                            in the lower part of the striker')]

        up_beam_stress, down_beam_stress = \
            self.compute_axial_bending_stress(variables)

        coordinates_x = self.get_plot_coordinates_x('element')
        time_discretization = np.linspace(0, variables['final_time'],
                                    up_beam_stress.shape[0], dtype=float)
        self.plot_field(up_beam_stress, coordinates_x, time_discretization,
                        variables['path'], fields_to_plot[0], 'MPa')
        self.plot_min_max_field(up_beam_stress, coordinates_x, \
                                variables['path'], fields_to_plot[2], 'MPa')

        self.plot_field(down_beam_stress, coordinates_x, time_discretization,
                        variables['path'], fields_to_plot[1], 'MPa')
        self.plot_min_max_field(down_beam_stress, coordinates_x, \
                                variables['path'], fields_to_plot[3], 'MPa')

        return fields_to_plot, captions_to_plot

    def draw_shaft_botom(self, figure_size_x, figure_size_y):
        """ Split the figure in 2 plots and draws the shaft geometry in the \
         second plot (ax2). It is used as axuxiliar plot, to easily recognize \
         the different sections. Then returns the axis (ax1) to plot theree \
         the intersting data.

        :param figure_size_x: width of the figure
        :type figure_size_x: double
        :param figure_size_x: height of the figure
        :type figure_size_x: double
        :return fig: handel of the figure
        :type fig: plt figure, mandatory
        :return ax1: handel of the axis of the upper part of the figure
        :type ax1: plt axis, mandatory
        """

        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')
        fig = plt.figure(figsize=(figure_size_x, figure_size_y))
        gs = gridspec.GridSpec(7, 1)
        ax1 = plt.subplot(gs[0:6])
        ax2 = plt.subplot(gs[6])

        ax2.plot(self.nodes[self.num_nodes_beam:self.num_nodes_beam * 2, 0],
                 self.nodes[self.num_nodes_beam:self.num_nodes_beam * 2, 1],
                 'k')
        ax2.plot(
            self.nodes[self.num_nodes_beam * 2:self.num_nodes_beam * 3, 0],
            self.nodes[self.num_nodes_beam * 2:self.num_nodes_beam * 3, 1],
            'k')
        ax2.set_xlabel(r'Position X [m]', rotation='horizontal')
        ax2.xaxis.set_major_formatter(mpl.ticker.StrMethodFormatter('{x:.3f}'))
        ax2.set_xlim(self.nodes[-1, 0], 0)
        ax2.set_yticklabels([])
        # ax2.axis('equal')
        #ax.margins(x=0)

        return fig, ax1

    def plot_field(self, field, coordinates_x, time_discretization,
                   path, file_image_name, units=None):
        """ Plot the field for all the times in a colour scale. The resulting \
        plots are stored in the folder image.

        :param field: vector with the values to be ploted
        :type field: numpy array
        :param coordinates_x: vector with the position in X direction to \
        make plots
        :type coordinates_x: numpy array
        :param time_discretization: vector with the time instants for each \
        time step
        :type time_discretization: numpy array
        :param field_to_read: path where the file is stored
        :type field_to_read: string, mandatory
        :param file_image_name: name to save the image in the image folder
        :type file_image_name: string
        :param units: unit to be added in the plot
        :type units: string
        """

        # Draw striker at the bottom and configure the grid image
        fig, ax1 = self.draw_shaft_botom(10, 5)
        # Draw field
        fig.subplots_adjust(top=0.95, bottom=0.05, left=0.10, right=0.78)
        contour = ax1.contourf(coordinates_x, time_discretization[1:],
                               field[1:,:], 126, cmap='viridis')
        ax1.set_xticklabels([])
        ax1.set_xlim(coordinates_x[-1],0)
        ax1.set_ylabel(r'Time [s]', rotation='vertical')

        # Add color bar
        cax = plt.axes([0.8, 0.15, 0.075, 0.8])
        cbar = fig.colorbar(contour, cax=cax, ax=ax1)
        cbar.ax.set_ylabel(units)

        save_image(plt, path, f'{file_image_name}')
        return

    def plot_min_max_field(self, field, coordinates_x, path, file_image_name,
                           units=None):
        """ Plot the minimum and maximum of the field in each section or node \
        for all the times in a colour scale. The resulting \
        plots are stored in the folder image.

        :param field: vector with the values to be ploted
        :type field: numpy array
        :param coordinates_x: vector with the position in X direction to \
        make plots
        :type coordinates_x: numpy array
        :param field_to_read: path where the file is stored
        :type field_to_read: string, mandatory
        :param file_image_name: name to save the image in the image folder
        :type file_image_name: string
        :param units: unit to be added in the plot
        :type units: string
        """
        field_max, field_min = get_field_maximums_and_minimums(field)

        # Draw striker at the bottom and configure the grid image
        fig, ax1 = self.draw_shaft_botom(7, 4)

        # Draw field
        ax1.plot(coordinates_x, field_max)
        ax1.plot(coordinates_x, field_min)
        ax1.set_xticklabels([])
        ax1.set_xlim(coordinates_x[-1], 0)
        ax1.set_ylabel(file_image_name.replace("_", " ") + f' [{units}]',
                       rotation='vertical')

        save_image(plt, path, f'{file_image_name}')
        return

    def plot_shaft_mesh(self, path):
        """Plot the shaft mesh used and it is stored in the image folder

        :param field_to_read: path where the file is stored
        :type field_to_read: string, mandatory
        """
        fig, ax1 = plt.subplots(figsize=(7, 2))
        fig.subplots_adjust(top=0.9, bottom=0.18, left=0.20, right=0.90)
        ax1.plot(self.nodes[self.num_nodes_beam:self.num_nodes_beam * 2, 0],
                 self.nodes[self.num_nodes_beam:self.num_nodes_beam * 2, 1],
                 color='blue')
        ax1.plot(self.nodes[self.num_nodes_beam * 2:self.num_nodes_beam * 3,
                 0], self.nodes[self.num_nodes_beam * 2:self.num_nodes_beam *
                                                        3, 1], color='orangered')
        ax1.set_xlabel(r'Position X [m]', rotation='horizontal')
        ax1.set_ylabel(r'Position Y [m]', rotation='vertical')
        ax1.axis('equal')
        ax1.grid()
        fig.tight_layout()

        num = self.num_nodes_beam
        num2 = self.num_nodes_beam * 2

        for i in range(0, num - 1):
            vert = [(self.nodes[num + i, 0], self.nodes[num + i, 1]),
                    (self.nodes[num + i + 1, 0], self.nodes[num + i + 1, 1]),
                    (self.nodes[num2 + i + 1, 0], self.nodes[num2 + i + 1, 1]),
                    (self.nodes[num2 + i, 0], self.nodes[num2 + i, 1])]
            poly = Polygon(vert, facecolor=f'1', edgecolor='0.5',
                           closed=True)
            ax1.add_patch(poly)

        ax1.annotate('A', xy=(-0.0118, 0.002), xytext=(-0.0118, 0.007),
                    arrowprops=dict(facecolor='black', shrink=0.005))
        ax1.annotate('B', xy=(-0.023, 0.002), xytext=(-0.023, 0.007),
                     arrowprops=dict(facecolor='black', shrink=0.005))
        ax1.annotate('C', xy=(-0.042, 0.002), xytext=(-0.042, 0.007),
                     arrowprops=dict(facecolor='black', shrink=0.005))
        ax1.annotate('D', xy=(-0.093, 0.001), xytext=(-0.093, 0.007),
                     arrowprops=dict(facecolor='black', shrink=0.005))
        save_image(plt, path, 'tmp_mesh')

        return

    def plot_stress_animation(self, field_to_read, variables):
        """ Make an animated chart along time of the stress vs the coordinate \
        in X. It is saved in .mp4 format.

        :param field_to_read: the name of the file to read and plot
        :type field_to_read: string
        :param variables: dictionary containing all the variables to define \
        the simulation, defined in define_model.py/define_variables()
        :type variables: dictionary
        """

        strain = read_results(field_to_read, 'element')
        stress = self.compute_stress(strain, field_to_read, variables)

        # Set figure, with two plots of different size
        plt.rc('text', usetex=True)
        plt.rc('font', family='serif')
        fig = plt.figure(figsize=(6, 4))
        gs = gridspec.GridSpec(7, 1)
        ax1 = plt.subplot(gs[0:6])
        ax2 = plt.subplot(gs[6])
        ax1.set_xlim(self.nodes[-1,0], 0)
        ax1.set_ylim(np.min(stress[1:]), np.max(stress[1:]))
        ax1.set_ylabel(' Stress (' + field_to_read.replace('_', ' ') +
                       ') [MPa]', rotation='vertical')
        ax2.set_xlabel(r'Position X [m]', rotation='horizontal')

        # Draw striker at the bottom
        # fig.subplots_adjust(top=0.99, bottom=0.15, left=0.20, right=0.95)
        self.draw_shaft_botom(ax2)

        # fig.subplots_adjust(top=0.99, bottom=0.05, left=0.20, right=0.95)
        line, = ax1.plot([], [], lw=3)
        time_template = 'time = %.5fs'
        time_text = ax1.text(0.05, 0.1, '', transform=ax1.transAxes)

        def init():
            line.set_data([], [])
            time_text.set_text('')
            return line, time_text

        def animate(i):
            x = np.linspace(0, self.nodes[-1,0], np.shape(stress)[1])
            line.set_data(x, stress[i, :])
            time_text.set_text(time_template % (variables['time_step']*i))
            return line, time_text

        ani = animation.FuncAnimation(fig, animate, np.arange(775, 1250),
                                      init_func=init,
                                      interval=100, blit=True)
        path = variables['path']
        ani.save(f'../{path}/stress_' + field_to_read + '.mp4', \
                 fps=30, extra_args=['-vcodec', 'libx264'])
