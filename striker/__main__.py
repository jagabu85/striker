from . import *

if __name__ == "__main__":

    oofelie_path = '~/OEbuild_oofelie/bin/oofelie'
    test = False

    actual_path = os.getcwd()
    results_path = actual_path + f'/../results'
    create_folders(results_path)

    several_simulations = {
        'bullet_stiffness': np.array([1.3e6, 0]),
        'name' : np.array(['bullet','no_bullet'])
    }

    #several_simulations = {'name':'r'}

    for i, name in enumerate(several_simulations['name']):
        print(name)
        simulation_name = {'path': f'{results_path}/' + name,
                           'name': 'striker',
                           'num_simulation': read_last_simulation_id(results_path),
                           'time': get_time_name(),
                           'simulation_title': ''
                           }

        geometry = define_geometry()
        variables = define_variables()

        variables['path'] = simulation_name['path'] \
        #                    + '_' + simulation_name['num_simulation']
        create_folders(variables['path'])

        variables['bullet_stiffness'] = \
            several_simulations['bullet_stiffness'][i]

        # Compute initial conditions hammer
        e_file = create_hammer_e_script(variables)
        save_e_file(e_file, variables['path'], 'hammer_compute_initial_condition')
        run_oofelie_script(oofelie_path, 'hammer_compute_initial_condition',
                          variables['path'])

        # Create striker oofelie file
        e_file, striker = create_striker_e_script(variables, geometry, test)
        save_e_file(e_file, variables['path'], simulation_name['name'])
        run_oofelie_script(oofelie_path, simulation_name['name'],
                          variables['path'])

        # Create pdf with results
        latex_file(striker, variables, geometry, simulation_name)

        # Save animations of the stress along time
        #striker.plot_stress_animation('gamma_1', variables)
        #striker.plot_stress_animation('gamma_2', variables)
        #striker.plot_stress_animation('gamma_3', variables)
        #striker.plot_stress_animation('kappa_1', variables)
        #striker.plot_stress_animation('kappa_2', variables)
        #striker.plot_stress_animation('kappa_3', variables)