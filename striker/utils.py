import math
import numpy as np
import os

def compute_circle_area(radius):
    return radius * radius * math.pi


def compute_inertia_circle(radius):
    return radius ** 4 * math.pi / 4


def compute_polar_inertia_circle(radius):
    return radius ** 4 * math.pi / 2


def read_file(folder_location, file_name):
    """ Read a file

    :param folder_location: folder where the file is stored
    :type folder_location: string, mandatory
    :param file_name: file name to be read
    :type file_name: string, mandatory
    :return read_data: array containing the read data
    :type read_data: numpy array
    """
    read_data = np.fromfile(folder_location + '/' + file_name, dtype=np.double)
    print(f'File {folder_location}/{file_name} read')
    return read_data


def create_folders(folder_to_create):
    """ Check if the folder exits, and if not it is created
    :param folder_to_create
    :return: True
    """

    if not (os.path.isdir(folder_to_create)):
        os.makedirs(folder_to_create)
        print('Folder created: ' + folder_to_create)
    else:
        print('Folder already exist: ' + folder_to_create)
    return True


def read_results(path, field_to_read, type):
    """ Read results from a binary file and reshape them.

    :param field_to_read: path where the file is stored
    :type field_to_read: string, mandatory
    :param field_to_read: file of the name to read
    :type field_to_read: string, mandatory
    :param type: 'node': if the field to plot is stored in the nodes the \
    shape of stress is [time_steps, num_nodes] \
    'element': if the field to plot is stored in the elements the \
    shape of stress is [time_steps, num_nodes-1]
    :type type: string, mandatory
    :return stress: vector with the stress in each one of the sections
    :type stress: numpy array
    """
    print('------------------------', type)
    file_name = 'numSteps.dat'
    num_steps = read_file(path, file_name)
    num_steps = np.int_(num_steps[0])

    file_name = 'numElements.dat'
    num_elements = read_file(path, file_name)
    num_elements = np.int_(num_elements[0])

    if type == "element":
        results = read_file(path, field_to_read + '.dat').\
            reshape(num_steps, num_elements)
    elif type == "node":
        results = read_file(path, field_to_read + '.dat'). \
            reshape(num_steps, num_elements+1)
    else:
        raise ValueError("The argument type in function "
                         "read_results is incorrect, it "
                         "should be: \"element\" or \"node\"")

    return results


def save_image(plt, path, name_image):
    """ Save the image in the folder Results.

    :return plt: handel of a figure
    :type plt: plt
    :return name_image: image name
    :type name_image: string
    """
    results_path = f'{path}/images'
    if not (os.path.isdir(results_path)):
        os.makedirs(results_path)
        print("Create images folder")
    plt.savefig(results_path + '/' + name_image + '.png')
    plt.close()
    return False


def get_field_maximums_and_minimums(field):
    """ Get the maximum and minimum values of the field for each node, or \
    each element. \
    The field starts in 1 to avoid the first values (t=0) of the strains which \
    are infinite and does not have physical meaning

    :return field_max: vector with the maximum values of the fields
    :type field_max: numpy array
    :return field_min: vector with the minimum values of the fields
    :type field_min: numpy array
    """
    field_max = np.zeros((np.shape(field)[1]))
    field_min = np.zeros((np.shape(field)[1]))
    for i in range(0, np.shape(field)[1]):
        field_max[i] = field[1:, i].max()
        field_min[i] = field[1:, i].min()
    return field_max, field_min


def run_oofelie_script(oofelie_bin_path, oofelie_script, path):
    """Change from the actual path to the path from where oofelie will be \
    launch and therefore where the data results will be saved

    :param oofelie_bin_path: Path where the oofelie executable is
    :type oofelie_bin_path: string, mandatory
    :param oofelie_script: name of the script to launch
    :type oofelie_script: string, mandatory
    :param path: name of the path where to save the .e and launch the \
    simulation
    :type path: string, optional
    :return: Bool to check if the function has been done
    :type: bool
    """

    oofelie_script += '.e'
    actual_path = os.getcwd()
    os.chdir(path)
    print((oofelie_bin_path + ' ' + oofelie_script +' > info.txt'))
    if os.path.exists(oofelie_script):
        os.system(oofelie_bin_path + ' ' + oofelie_script + '  > info.txt')
    else:
        raise ValueError('Error: The script to run ' + oofelie_script + ' does '
                         'not exist')
    os.chdir(actual_path)
    return False


def read_last_simulation_id(path):

    if os.path.isfile(path + 'simulation_ID.txt') :
        file1 = open(path + '/simulation_ID.txt', "r")
        simulation_id = file1.read() + 1
        file1.close()
        print('Simulation read: ', simulation_id)
    else:
        simulation_id = '1'
        file1 = open(path + "/simulation_ID.txt", "w")
        file1.write(simulation_id)
        file1.close()

    return simulation_id