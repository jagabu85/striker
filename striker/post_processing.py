from pylatex import Document, Section, Subsection, Tabular, Math, TikZ, Axis, \
    Plot, Figure, Matrix, Alignat, Command, LongTable, PageStyle, Head, Foot, \
    MiniPage, StandAloneGraphic, MultiColumn, Tabu, LongTabu, LargeText, \
    MediumText, LineBreak, NewPage, Tabularx, TextColor, simple_page_number
from pylatex.utils import italic, NoEscape, bold
import time
import matplotlib.pyplot as plt

from .utils import *
from .create_striker_e_file import create_striker_e_script
from .define_model import compute_time_discretization_striker_simulation

def get_time_name():
    return time.strftime("%Y%m%d-%H%M%S")


def latex_title(doc, title):
    """ Make the title of the tex file.

    :param doc: Document object (contain all the info to create the tex and \
    the pdf file
    :type doc: Document, mandatory
    :param title: Title for the pdf file
    :type title: string, mandatory
    """
    # Make title
    doc.preamble.append(Command('title', NoEscape(title + r'\vspace{-1cm}')))
    #doc.preamble.append(Command('author', 'XXXX XXXX'))
    doc.preamble.append(Command('date', NoEscape(r'\today')))
    doc.append(NoEscape(r'\maketitle'))
    # End first page style
    return


def create_table_sections(doc, geometry):
    """ Create a table in the pdf file with all the values of the striker \
    geometry. It means the data of the numpy array used to initialize the \
    shaft object.

    :param doc: Document object (contain all the info to create the tex and \
    the pdf file
    :type doc: Document, mandatory
    :param geometry: geometry description to be used for the class shaft
    :type geometry: numpy array, mandatory
    """
    with doc.create(Section('Model definition')):
        with doc.create(
                Subsection('Striker discretization with beam elements')):
            with doc.create(LongTable("l c c c c c")) as data_table_sections:
                data_table_sections.add_hline()
                data_table_sections.add_row(['Id section', 'Initial radius',
                                             'Final radius', 'Length',
                                             'Number of elements',
                                             'Contact radius'])
                data_table_sections.add_hline()
                data_table_sections.end_table_header()
                data_table_sections.add_hline()
                data_table_sections.add_row((MultiColumn(6, align='r',
                                             data='Continued on Next Page'),))
                data_table_sections.add_hline()
                data_table_sections.end_table_footer()
                data_table_sections.add_hline()
                data_table_sections.end_table_last_footer()
                for i, section in enumerate(geometry):
                    data_table_sections.add_row(
                        [f'Section {i + 1}:', f'{section[0]:.6f}',
                         f'{section[1]:.6f}',
                         f'{section[2]:.6f}',
                         f'{section[3]:.8f}',
                         f'{section[4]}'])
    return


def create_table_variables(doc, variables):
    """ Create a table in the pdf file with all the values of the variables. \
    It means the data of the numpy array defined in define_variables()

    :param doc: Document object (contain all the info to create the tex and \
    the pdf file
    :type doc: Document, mandatory
    :return variables: Dictionary with all the variables need to define the \
    geometries of the model (except the striker) and the numerical parameters.
    :type variables: Dictionary
    """
    with doc.create(Subsection('Variables used for the simulation')):
        # doc.append('Variables used for the simulation.')

        # Generate data table
        with doc.create(LongTable("l l")) as data_table_variables:
            data_table_variables.add_hline()
            data_table_variables.add_row(["Variable[key]", "Value"])
            data_table_variables.add_hline()
            data_table_variables.end_table_header()
            data_table_variables.add_hline()
            data_table_variables.add_row((MultiColumn(2, align='r',
                                            data='Continued on Next Page'),))
            data_table_variables.add_hline()
            data_table_variables.end_table_footer()
            data_table_variables.add_hline()
            #data_table_variables.add_row((MultiColumn(2, align='r',
            #                                data='End of table'),))
            #data_table_variables.add_hline()
            data_table_variables.end_table_last_footer()
            for key, value in variables.items():
                if isinstance(value, np.ndarray):
                    for row_in_value in value:
                        data_table_variables.add_row([key, row_in_value])
                else:
                    data_table_variables.add_row([key, value])
    return


def get_field_name(field):
    """ Returns the name of the stress and the direction for a given strain \
    name.

    :param field: name of the strain
    :type field: string, mandatory
    :return file_name: get the long nice name of the stress for each strain
    :type file_name: string
    """
    if field == 'gamma_1':
        field_name = 'Axial stress in X'
    elif field == 'gamma_2':
        field_name = 'Shear stress in Z'
    elif field == 'gamma_3':
        field_name = 'Shear stress in Y'
    elif field == 'kappa_1':
        field_name = 'Torsion stress in X'
    elif field == 'kappa_2':
        field_name = 'Bending stress in Z'
    elif field == 'kappa_3':
        field_name = 'Bending stress in Y'
    else:
        field_name = field

    return field_name


def add_field_plots(doc, path, fields_to_plot, field_detail='', width='14cm'):
    """ Add the images saved in png format to the pdf.

    :param doc: Document object (contain all the info to create the tex and \
    the pdf file
    :type doc: Document, mandatory
    :param fields_to_plot: name of the field, example strain, velocity, \
                           displacement...
    :type fields_to_plot: string, mandatory
    :type field_detail: to specified details about the field, example: _min_max
    :type field_detail: string, optional
    :param width: the with of the image, should have units, e.g. '10cm'
    :type width: string, optional
    """
    for i, field in enumerate(fields_to_plot):
        pdf_caption = f' Field of {field[:-1].replace("_", " ")} in ' \
                              f'the {field[-1].title()}  direction'

        # Define the caption to be used
        field_name = get_field_name(field)
        if field_name != field:
            pdf_caption += f', ({field_name})'

        file_name = f'tmp_{field}{field_detail}'
        add_image(doc, path, file_name, pdf_caption, width)


def add_image(doc, path, image_name, image_caption = None, width= '14cm'):
    """ Add an images in png format to the pdf.

    :param doc: Document object (contain all the info to create the tex and \
    the pdf file
    :type doc: Document, mandatory
    :param image_name: name of the image to add
    :type image_name: string, mandatory
    :param image_caption: Caption used for the figure label
    :type image_caption: string, optional
    :param width: the with of the image, should have units, e.g. '10cm'
    :type width: string, optional
    """
    with doc.create(Figure(position='h!')) as field_pictures:
        image = path + "/images/"+ image_name + ".png"
        field_pictures.add_image(image, width=width)
        if image_caption == None:
            field_pictures.add_caption('')
        else:
            field_pictures.add_caption(image_caption)


def create_length_plot(variables):
    """ Creates a plot of the Striker length, and the displacements of the \
    firsta and last node of the striker along time. It is saved in the folder \
    images with the name 'tmp_striker_length.png'

    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandatory
    """
    displacements = read_results(variables['path'], 'displacements_x', 'node')

    time_discretization = compute_time_discretization_striker_simulation(variables)

    fig, ax1 = plt.subplots(figsize=(7, 4))
    fig.subplots_adjust(top=0.99, bottom=0.15, left=0.20, right=0.9)
    plt.plot(time_discretization, displacements[:, 0],
             label='First node displacement')
    plt.plot(time_discretization, displacements[:, 200],
             label='Last node displacement')
    plt.plot(time_discretization, displacements[:, 0] - displacements[:, 200],
             label='Striker length')
    ax1.set_xlabel(r'Time [s]', rotation='horizontal')
    ax1.set_ylabel(r'[m]', rotation='vertical')
    plt.legend(loc='lower right', shadow=True, bbox_to_anchor=(1.1, 0.1))

    if variables['bullet_stiffness'] == 0:
        plot_contact_instants_no_bullet(ax1, variables)
    else:
        plot_contact_instants_bullet(ax1, variables)

    save_image(plt, variables['path'], 'tmp_striker_length')


def plot_contact_instants_bullet(ax1, variables):
    """ Add vertical lines to the figure. It is used to show the impact \
     instants of the striker.
     WARNING!! This image should be adjusted for each simulation

    :param ax1: handel of the axis of the figure
    :type ax1: plt axis, mandatory
    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandtory
    """
    final_time_hammer = variables['hammer_time_step'] * \
                        variables['hammer_num_steps']
    # 1nd contact hammer
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 11),c='r')
    # tip bullet
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 38))
    # 2nd contact hammer
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 69), c='r')
    # 3nd contact hammer
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 84), c='r')
    # backwards stoper
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 192), c='k')


def plot_contact_instants_no_bullet(ax1, variables):
    """ Add vertical lines to the figure. It is used to show the impact \
     instants of the striker.
     WARNING!! This image should be adjusted for each simulation

    :param ax1: handel of the axis of the figure
    :type ax1: plt axis, mandatory
    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandtory
    """
    final_time_hammer = variables['hammer_time_step'] * \
                        variables['hammer_num_steps']
    # 1nd contact hammer
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 12), c='r')
    #forward stopper
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 79),  c='b')
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 118), c='b')
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 126), c='b')
    # 2nd contact hammer
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 103), c='r')
    # 3nd contact hammer
    ax1.axvline(x=final_time_hammer + (variables['time_step'] * 122), c='r')
    # backwards stoper
    #ax1.axvline(x=final_time_hammer + (variables['time_step'] * 1), c='k')


def plot_axial_benging_over_time(variables, striker, element):
    """ Creates a plot of the stress (axial+benging) along time for one \
    choosen element. The image it is saved in the folder 'images' with the \
    name plot_axial_benging_over_time_{element}.

    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandatory
    :param striker: Shaft object, with the simulated geometry
    :type striker: Shaft, mandatory
    :param element: Choose the element you want ot plot
    :type element: int, mandatory
    """
    up_beam_stress, down_beam_stress = \
        striker.compute_axial_bending_stress(variables)

    time_discretization = compute_time_discretization_striker_simulation(variables)

    fig, ax1 = plt.subplots(figsize=(7, 4))
    #fig.subplots_adjust(top=0.99, bottom=0.15, left=0.20, right=0.9)
    plt.plot(time_discretization, up_beam_stress[:, element], \
             label = 'upper part')
    plt.plot(time_discretization, down_beam_stress[:, element], \
             label = 'lower part')
    ax1.set_xlabel(r'Time [s]', rotation='horizontal')
    ax1.set_ylabel(r'Stress (Axial+Bending) [MPa]', rotation='vertical')

    plt.legend(loc='lower right', shadow=True)
    #plt.legend(loc='lower right', shadow=True, bbox_to_anchor=(1.1, 0.1))
    save_image(plt, variables['path'], \
               f'axial_benging_stress_over_time_{element}')

def plot_field_in_section(field, field_name, axis_y_label, node, variables):
    """ Creates a plot of a field along time for one choosen element or node.
    The image it is saved in the folder 'images'

    :param field:
    :type field: numpy array, mandatory
    :param field_name: string used in the construction of the name image file
    :type field_name: string, mandatory
    :param axis_y_label: String to show down the y axis in the chart
    :type axis_y_label: string, mandatory
    :param node: Choose the element or node (depending the field type) you \
    want ot plot
    :type node: int, mandatory
    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandatory
    """
    time_discretization = compute_time_discretization_striker_simulation(variables)
    fig, ax1 = plt.subplots(figsize=(7, 4))
    plt.plot(time_discretization, field[:, node])
    ax1.set_xlabel(r'Time [s]', rotation='horizontal')
    ax1.set_ylabel(f'{axis_y_label}', rotation='vertical')

    save_image(plt, variables['path'], f'{field_name}_over_time_{node}')

def plot_axial_bending_over_time_3fig(striker, doc, variables, axial_stress, \
                                      bending_stress, section):
    section = striker.section_list[section].id_ini
    plot_axial_benging_over_time(variables, striker, section)
    add_image(doc,  variables['path'], \
              f'axial_benging_stress_over_time_{section}', \
              f'Stress (axial+bending) along time in the element {section}')

    plot_field_in_section(axial_stress, 'axial_stress', 'Stress (MPA)', \
                          section, variables)
    add_image(doc, variables['path'], f'axial_stress_over_time_{section}', \
              f'Axial stress along time in the element {section}')

    plot_field_in_section(bending_stress, 'bending_stress', 'Stress (MPA)', \
                          section, variables)
    add_image(doc, variables['path'], f'bending_stress_over_time_{section}', \
              f'Bending stress along time in the element {section}')

def latex_file(striker, variables, geometry, file_name):
    """ Read the data stored by Oofelie, and creates a report of the results \
    in pdf (latex) inside the results folder. It stores the variables and \
    geometry used, then shows the grid used, and show several plots of the \
    results.

    :param striker: Shaft object, with the simulated geometry
    :type striker: Shaft, mandatory
    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandatory
    :param geometry: geometry description to be used for the class shaft
    :type geometry: numpy array, mandatory
    :param file_name: dictionary with the necessary info to create the pdf \
    file name
    :type file_name: dictionary, mandatory
    """
    geometry_options = {
        "left": "2.2cm",
        "right": "2.2cm",
        "top": "0cm",
        "bottom": "1cm",
        "includeheadfoot": True
    }
    doc = Document(geometry_options=geometry_options)
    
    # Make title
    latex_title(doc, 'Striker simulation: ' + file_name['time'] + '  ' +
                file_name['simulation_title'])
    create_table_sections(doc, geometry)
    create_table_variables(doc, variables)

    with doc.create(Section('Mesh discretization')):
        striker.plot_shaft_mesh(variables['path'])

        with doc.create(Figure(position='h!')) as mesh_picture:
            mesh_picture.add_image(variables['path']+'/images/tmp_mesh.png',
                                   width='12cm')
            pdf_caption = f' Mesh discretization'
            mesh_picture.add_caption(pdf_caption)
            doc.append(NoEscape(r'\clearpage'))

    with doc.create(Section('Results')):
        # ------------------------------------------ Add axial + bending stress
        with doc.create(Subsection('Stress: Axial + Bendign')):
            images_to_add, captions_to_plot = \
                striker.plot_axial_bending_stress_field(variables)

            for i, image_to_add in enumerate(images_to_add):
                add_image(doc, variables['path'], image_to_add, \
                          captions_to_plot[i])
        doc.append(NoEscape(r'\clearpage'))

        # -------------------------------- Add axial + bending stress along time
        with doc.create(Subsection('Stress in a section along time')):
            axial_strain = read_results(variables['path'], 'gamma_1', 'element')
            axial_stress = striker.compute_stress(axial_strain, 'gamma_1', \
                                                  variables)

            bending_strain = read_results(variables['path'], 'kappa_2', 'element')
            bending_stress = striker.compute_stress(bending_strain, 'kappa_2',\
                                                    variables)
            with doc.create(Subsection('Section A')):
                plot_axial_bending_over_time_3fig(striker, doc, variables, \
                                            axial_stress, bending_stress, 4)
            doc.append(NoEscape(r'\clearpage'))

            with doc.create(Subsection('Section B')):
                plot_axial_bending_over_time_3fig(striker, doc, variables, \
                                            axial_stress, bending_stress, 8)
            doc.append(NoEscape(r'\clearpage'))

            with doc.create(Subsection('Section C (Breaking point guide section)')):
                plot_axial_bending_over_time_3fig(striker, doc, variables, \
                                            axial_stress, bending_stress, 12)
            doc.append(NoEscape(r'\clearpage'))

            with doc.create(Subsection('Section D (Breaking point tip section)')):
                plot_axial_bending_over_time_3fig(striker, doc, variables, \
                                            axial_stress, bending_stress, 18)
            doc.append(NoEscape(r'\clearpage'))
        doc.append(NoEscape(r'\clearpage'))

        # --------------------------------------------------- Add stress charts
        with doc.create(Subsection('Stress')):
            fields_to_plot = [('gamma_1'),
                              # ('gamma_2'),
                              ('gamma_3'),
                              # ('kappa_1'),
                              ('kappa_2')]  # ,
            # ('kappa_3')]
            for field in fields_to_plot:
                striker.create_plots_field(field, variables, 'element', 'MPa')
            add_field_plots(doc, variables['path'], fields_to_plot)
        doc.append(NoEscape(r'\clearpage'))

        with doc.create(Subsection('Maximum and minimum stresses')):
            add_field_plots(doc, variables['path'], fields_to_plot, \
                            '_min_max', '10cm')
        doc.append(NoEscape(r'\clearpage'))

        # ------------------------------------------------- Add velocity charts
        with doc.create(Subsection('Velocities')):
            fields_to_plot = [('velocities_x'),
                              ('velocities_y')]
            for field in fields_to_plot:
                striker.create_plots_field(field, variables, 'node', 'm/s')
            add_field_plots(doc, variables['path'], fields_to_plot)
        doc.append(NoEscape(r'\clearpage'))

        with doc.create(Subsection('Velocities: Maximum and minimum')):
            add_field_plots(doc, variables['path'], fields_to_plot, '_min_max',\
                            '10cm')
        doc.append(NoEscape(r'\clearpage'))

        # --------------------------------------------- Add displacemnts charts
        with doc.create(Subsection('Displacements')):
            fields_to_plot = [('displacements_x'),
                              ('displacements_y')]
            for field in fields_to_plot:
                striker.create_plots_field(field, variables, 'node', 'm')
            add_field_plots(doc, variables['path'], fields_to_plot)
        doc.append(NoEscape(r'\clearpage'))

        with doc.create(Subsection('Displacements: Maximum and minimum')):
            add_field_plots(doc, variables['path'], fields_to_plot, \
                            '_min_max', '10cm')
        doc.append(NoEscape(r'\clearpage'))
        
        # --------------------------------------------- Add length striker chart
        with doc.create(Subsection('Striker displacements and length variation')):
            image_name = 'tmp_striker_length'
            create_length_plot(variables)
            add_image(doc, variables['path'], image_name, 'Striker length', \
                      '10cm')
        doc.append(NoEscape(r'\clearpage'))

        # --------------------------------------------- End results section

    with doc.create(Section('Annex:')):
        oofelie_script, _ = create_striker_e_script(variables, geometry)
        doc.append(oofelie_script)

    doc.generate_pdf(variables['path'] +'/results' , clean_tex=False)
    print('------------------------------------------------ Pdf results saved')
    return


############################################################################
#Not used for the pdf latex, only to create the comparison image for the
#report
def compute_axial_bending_stress(shaft, variables, path):
        """ Comput the max and min stress combining the axial and the bending \
        Note: Negative strain means negative bending, deformation _--_
              Positive strain means positive bending, deformation -_-
        stress:
            upper part of the beam = gamma_1 (axial) + kappa_3 (bending z)
            lower part of the beam = gamma_1 (axial) - kappa_3 (bending z)

        :param shaft: object class shaft, should have the properties of the \
        results to plot
        :type shaft: object shaft
        :param variables: dictionary containing all the variables to define \
        the simulation, defined in define_model.py/define_variables()
        :type variables: dictionary, mandatory
        :return path: path where the results (file.dat) are stored
        :type path: string, mandatory
        """
        axial_strain = read_results(path, 'gamma_1', 'element')
        bending_strain = read_results(path, 'kappa_2', 'element')

        axial_stress = shaft.compute_stress(axial_strain, 'gamma_1', \
                                           variables)
        # this bending_stress is the upper part of the beam
        bending_stress = shaft.compute_stress(bending_strain, 'kappa_3', \
                                             variables)

        up_beam_stress = axial_stress + bending_stress
        down_beam_stress = axial_stress - bending_stress

        return up_beam_stress, down_beam_stress

def plot_comparison_axial_benging_up_over_time(variables, striker, element):
    """ Creates a plot of the stress (axial+benging) along time for one \
    choosen element. The image it is saved in the folder 'images' with the \
    name plot_axial_benging_over_time_{element}.

    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandatory
    :param striker: Shaft object, with the simulated geometry
    :type striker: Shaft, mandatory
    :param element: Choose the element you want ot plot
    :type element: int, mandatory
    """

    path_1st_simulation = '/home/javier/simulations/striker/results/bullet'
    path_2dn_simulation = '/home/javier/simulations/striker/results/no_bullet'
    up_beam_stress_1st, down_beam_stress_1st = \
        compute_axial_bending_stress(striker, variables, path_1st_simulation)
    up_beam_stress_2nd, down_beam_stress_2nd = \
        compute_axial_bending_stress(striker, variables, path_2dn_simulation)

    time_discretization = compute_time_discretization_striker_simulation(variables)

    fig, ax1 = plt.subplots(figsize=(7, 4))
    #fig.subplots_adjust(top=0.99, bottom=0.15, left=0.20, right=0.9)
    plt.plot(time_discretization, up_beam_stress_1st[:, element], \
             '--b', label = 'upper part with bullet')
    plt.plot(time_discretization, up_beam_stress_2nd[:, element], \
             'r', label='upper part no bullet')
    ax1.set_xlabel(r'Time [s]', rotation='horizontal')
    ax1.set_ylabel(r'Stress (Axial+Bending) [MPa]', rotation='vertical')

    plt.legend(loc='lower right', shadow=True)
    #plt.legend(loc='lower right', shadow=True, bbox_to_anchor=(1.1, 0.1))
    save_image(plt, variables['path'], \
               f'comparison_axial_benging_stress_over_time_up_{element}')

def plot_comparison_axial_benging_low_over_time(variables, striker, element):
    """ Creates a plot of the stress (axial+benging) along time for one \
    choosen element. The image it is saved in the folder 'images' with the \
    name plot_axial_benging_over_time_{element}.

    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary, mandatory
    :param striker: Shaft object, with the simulated geometry
    :type striker: Shaft, mandatory
    :param element: Choose the element you want ot plot
    :type element: int, mandatory
    """

    path_1st_simulation = '/home/javier/simulations/striker/results/bullet'
    path_2dn_simulation = '/home/javier/simulations/striker/results/no_bullet'
    up_beam_stress_1st, down_beam_stress_1st = \
        compute_axial_bending_stress(striker, variables, path_1st_simulation)
    up_beam_stress_2nd, down_beam_stress_2nd = \
        compute_axial_bending_stress(striker, variables, path_2dn_simulation)

    time_discretization = compute_time_discretization_striker_simulation(variables)

    fig, ax1 = plt.subplots(figsize=(7, 4))
    #fig.subplots_adjust(top=0.99, bottom=0.15, left=0.20, right=0.9)
    plt.plot(time_discretization, down_beam_stress_1st[:, element], \
             '--b',     label = 'lower part with bullet')
    plt.plot(time_discretization, down_beam_stress_2nd[:, element], \
             'r', label='lower part no bullet')
    ax1.set_xlabel(r'Time [s]', rotation='horizontal')
    ax1.set_ylabel(r'Stress (Axial+Bending) [MPa]', rotation='vertical')

    plt.legend(loc='lower right', shadow=True)
    #plt.legend(loc='lower right', shadow=True, bbox_to_anchor=(1.1, 0.1))
    save_image(plt, variables['path'], \
               f'comparison_axial_benging_stress_over_time_lower_{element}')
################################################################################

def plot_comparison(striker, variables, geometry, file_name):
    element = striker.section_list[4].id_ini
    plot_comparison_axial_benging_up_over_time(variables, striker, element)
    plot_comparison_axial_benging_low_over_time(variables, striker, element)
    element = striker.section_list[8].id_ini
    plot_comparison_axial_benging_up_over_time(variables, striker, element)
    plot_comparison_axial_benging_low_over_time(variables, striker, element)
    element = striker.section_list[12].id_ini
    plot_comparison_axial_benging_up_over_time(variables, striker, element)
    plot_comparison_axial_benging_low_over_time(variables, striker, element)