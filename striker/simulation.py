import numpy as np
import math


class Simulation:
    """ This class is used to track the last IDs for elements and nodes used \
    in the oofelie script created in the function create_e_script()

    :param last_oofelie_node: Counter, store the number of nodes already \
    used in the oofelie script
    :type last_oofelie_node: int, mandatory
    :param last_oofelie_element: Counter, store the number of elements \
    already used in the oofelie script
    :type last_oofelie_element: int, mandatory
    :param id_nodes: Dictionary used to give an understandable label to each \
    important node and be possible to make reference to it later.
    :type id_nodes: dictionary, optional
    """

    def __init__(self):
        """Constructor method
        """
        self.last_oofelie_node = 0  # Last node id used in the oofelie script
        self.last_oofelie_element = 0   # Last element id used in the oofelie
                                        # script
        self.id_nodes = {}  # Store the important id nodes with a meaning name
                            # id_nodes['node_name'] = id

    def get_next_oofelie_node(self, dict_key = None):
        """ Add 1 to the counter self.last_oofelie_node,
        Safe the value of self.last_oofelie_node in the dictionary self.id_nodes

        :param dict_key: label for the node
        :type dict_key: string
        :return: Return the status of the counter self.last_oofelie_node
        :rtype: int
        """
        self.last_oofelie_node = self.last_oofelie_node + 1
        if dict_key is not None:
            self.id_nodes[dict_key] = self.last_oofelie_node
        return self.last_oofelie_node

    def get_next_oofelie_element(self):
        """ Add 1 to the counter last_oofelie_element,

        :return: Return the status of the counter self.last_oofelie_element
        :rtype: int
        """
        self.last_oofelie_element = self.last_oofelie_element + 1
        return self.last_oofelie_element