.. _intro-label:

Introduction
============

The striker package is a Python package to make the pre and post process to perform FEM simulations with Oofelie Multiphysics. It is not thought to be used as a general tool, instead it is developed for an specific use. Nevertheless, it can be useful to simulate different mechanical shaft geometries.

The current implementation has been developed in Python 3.6.5 :: Anaconda, Inc.

Discretization
**************

.. tikz::

	%upper shaft
	\draw[line width=0.4mm, dashed] (-0.5,0)--(5,0);

    \draw[line width=0.6mm](0,0)--(0,0.5)--(1,0.5)--
	(2,1)--(3.5,1)--(3.5,0.5)--(4.5,0.5)--(4.5,0);

	\draw[line width=0.6mm](0,0)--(0,-0.5)--(1,-0.5)--
	(2,-1)--(3.5,-1)--(3.5,-0.5)--(4.5,-0.5)--(4.5,0);

	\draw[line width=0.6mm] (1,0.5)--(1,-0.5);
	\draw[line width=0.6mm] (2,1)--(2,-1);
	\draw[line width=0.6mm] (3.5,1)--(3.5,-1);

	\node[color=red] at (0.5,0) (nodeS) {1};
	\node[color=red] at (1.5,0) (nodeS) {2};
	\node[color=red] at (2.75,0) (nodeS) {3};
	\node[color=red] at (4,0) (nodeS) {4};

	%lower discretization
	\draw[line width=0.4mm, dashed] (-0.5,-3)--(5,-3);

	% Beam nodes
	\draw[line width=0.4mm] (0,-3)--(4.5,-3);
	\draw[fill=black] (0,  -3) circle (0.07cm);
	\draw[fill=black] (0.5,-3) circle (0.07cm);
	\draw[fill=black] (1,  -3) circle (0.07cm);
	\draw[fill=black] (1.5,-3) circle (0.07cm);
	\draw[fill=black] (2,  -3) circle (0.07cm);
	\draw[fill=black] (2.5,-3) circle (0.07cm);
	\draw[fill=black] (3,  -3) circle (0.07cm);
	\draw[fill=black] (3.5,-3) circle (0.07cm);
	\draw[fill=black] (4,  -3) circle (0.07cm);
	\draw[fill=black] (4.5,-3) circle (0.07cm);

	%Upper nodes
	\draw[fill=red, color = red] (0,-2.5) circle (0.07cm);
	\draw[fill=red, color = red] (0.5,-2.5) circle (0.07cm);
	\draw[fill=red, color = red] (1,-2.5) circle (0.07cm);
	\draw[fill=red, color = red] (1.5,-2.25) circle (0.07cm);
	\draw[fill=red, color = red] (2,-2) circle (0.07cm);
	\draw[fill=red, color = red] (2.5,-2) circle (0.07cm);
	\draw[fill=red, color = red] (3,-2) circle (0.07cm);
	\draw[fill=red, color = red] (3.5,-2) circle (0.07cm);
	\draw[fill=red, color = red] (4,-2.5) circle (0.07cm);
	\draw[fill=red, color = red] (4.5,-2.5) circle (0.07cm);

	%Lower nodes
	\draw[fill=red, color = red] (0,  -3.5) circle (0.07cm);
	\draw[fill=red, color = red] (0.5,-3.5) circle (0.07cm);
	\draw[fill=red, color = red] (1,  -3.5) circle (0.07cm);
	\draw[fill=red, color = red] (1.5,-3.75) circle (0.07cm);
	\draw[fill=red, color = red] (2,  -4) circle (0.07cm);
	\draw[fill=red, color = red] (2.5,-4) circle (0.07cm);
	\draw[fill=red, color = red] (3,  -4) circle (0.07cm);
	\draw[fill=red, color = red] (3.5,-4) circle (0.07cm);
	\draw[fill=red, color = red] (4,  -3.5) circle (0.07cm);
	\draw[fill=red, color = red] (4.5,-3.5) circle (0.07cm);

	%Rigid element lines
	\draw[line width=0.4mm, color = red] (0,-2.5)--(0,-3.5);
	\draw[line width=0.4mm, color = red] (0.5,-2.5)--(0.5,-3.5);
	\draw[line width=0.4mm, color = red] (1,-2.5)--(1,-3.5);
	\draw[line width=0.4mm, color = red] (1.5,-2.25)--(1.5,-3.75);
	\draw[line width=0.4mm, color = red] (2,-2)--(2,-4);
	\draw[line width=0.4mm, color = red] (2.5,-2)--(2.5,-4);
	\draw[line width=0.4mm, color = red] (3,-2)--(3,-4);
	\draw[line width=0.4mm, color = red] (3.5,-2)--(3.5,-4);
	\draw[line width=0.4mm, color = red] (4,-2.5)--(4,-3.5);
	\draw[line width=0.4mm, color = red] (4.5,-2.5)--(4.5,-3.5);

	%Contact
	\draw[line width=0.8mm] (1.75,-1.8)--(3.75,-1.8);
	\draw[fill=red, color = black] (2.75,-1.8) circle (0.1cm);

	\draw[line width=0.8mm] (1.75,-4.2)--(3.75,-4.2);
	\draw[fill=red, color = black] (2.75,-4.2) circle (0.1cm);

	% Table
	\node[color=black] at (8.5,-1) (nodeS)
	{
		\begin{tabular}{ccccc}
		\hline
		$r_{init}$ & $r_{end}$ & $Length$ & $\sharp ele$ & $r_{contact}$\\
		\hline
		0.5 &0.5 &1   &2 &None\\
		0.5 &1   &2   &2 &None\\
		1   &1   &1.5 &3 &1.2\\
		0.5 &0.5 &1   &2 &None\\
		\hline
		\end{tabular}
	};

Limitation
============
.. warning::

   WARNING !!! FOR TWO CONSECUTIVE SECTIONS WITH CONTACT IT DOES NOT WORK'

