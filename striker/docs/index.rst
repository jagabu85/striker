Welcome to Striker's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
