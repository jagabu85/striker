import numpy as np
import math


class Section:
    """ Contains the information needed to discretize one section of the shaft.
    A section understood as a part of the shaft with the same radius, or with a
    constant slope between two different radius.

    .. tikz::

        %upper shaft
        \draw[line width=0.4mm, dashed] (1.5,0)--(4,0);
        \draw[line width=0.4mm, dashed] (4.5,0)--(7,0);

        \draw[line width=0.6mm](2,-1)--(2,1)--(3.5,1)--(3.5,-1);
        \draw[line width=0.6mm](2,-1)--(3.5,-1);

        % Beam nodes
        \draw[fill=black] (5,  -0) circle (0.07cm);
        \draw[fill=black] (5.5,-0) circle (0.07cm);
        \draw[fill=black] (6,  -0) circle (0.07cm);
        \draw[fill=black] (6.5,-0) circle (0.07cm);

        \draw[line width=0.6mm](5,0)--(6.5,0);

        %Upper nodes
        \draw[fill=red, color = blue] (5, 1) circle (0.07cm);
        %\draw[fill=red, color = red] (5.5, 1) circle (0.07cm);
        %\draw[fill=red, color = red] (6, 1) circle (0.07cm);
        \draw[fill=red, color = orange] (6.5, 1) circle (0.07cm);

        %Lower nodes
        \draw[fill=red, color = black] (5,  -1) circle (0.07cm);
        %\draw[fill=red, color = red] (5.5,-1) circle (0.07cm);
        %\draw[fill=red, color = red] (6,  -1) circle (0.07cm);
        \draw[fill=red, color = black] (6.5,-1) circle (0.07cm);

        %Rigid element lines
        \draw[line width=0.4mm, color = black] (5, 1)--(5,-1);
        %\draw[line width=0.4mm, color = red] (5.5, 1)--(5.5,-1);
        %\draw[line width=0.4mm, color = red] (6, 1)--(6,-1);
        \draw[line width=0.4mm, color = black] (6.5, 1)--(6.5,-1);

        %Contact
        \draw[line width=0.8mm] (4.75,1.2)--(6.75,1.2);
        \draw[fill=red, color = black] (5.75,1.2) circle (0.1cm)node\
        [anchor=south] {$r_{contact}$};

        \draw[line width=0.8mm] (4.75,-1.2)--(6.75,-1.2);
        \draw[fill=red, color = black] (5.75,-1.2) circle (0.1cm);

        \draw[line width=0.4mm, color = blue] (5,0)--(5,1)node[anchor=west] \
        {$r_{ini}$};
        \draw[line width=0.4mm, color = orange] (6.5,0)--(6.5,1)\
        node[anchor=west] {$r_{end}$};
        \draw (7,-1)node[anchor=west] {$num\_elements=3$};
        \draw[line width=0.4mm, color = red] (5,0)--(6.5,0)\
        node[anchor=north] {$length$};

    :param radius_ini: radius at the beginning of the section [m]
    :type radius_ini: double, mandatory
    :param radius_end: radius at the end of the section [m]
    :type radius_end: double, mandatory
    :param length: length of the section in [m]
    :type length: double, mandatory
    :param num_elements: number of beam elements used to discretize the section
    :type num_elements: int, mandatory
    :param radius_contact: radius of the guide system for the striker
    :type radius_contact: double, mandatory
    :param id_contact_up: Id used in the Oofelie scprit for the upper contact \
    node
    :type id_contact_up: int, optional
    :param id_contact_down: Id used in the Oofelie scprit for the lower \
    contact node
    :type id_contact_down: int, optional
    :param id_ini: Id used in the Oofelie scprit for the first beam node
    :type id_ini: int, optional
    :param id_end: Id used in the Oofelie scprit for the last beam node
    :type id_end: int, optional
    :param id_ini_up: Id used in the Oofelie scprit for the first upper node \
    used to define the rigid elements node
    :type id_ini_up: int, optional
    :param id_end_up: Id used in the Oofelie scprit for the last upper node \
    used to define the rigid elements
    :type id_end_up: int, optional
    :param id_ini_down: Id used in the Oofelie scprit for the first lower \
    node used to define the rigid elements
    :type id_ini_down: int, optional
    :param id_end_up: Id used in the Oofelie scprit for the last lower node \
    used to define the rigid elements
    :type id_end_down: int, optional
    """

    def __init__(self, radius_ini, radius_end, length, num_elements,
                 radius_contact=None):
        """Constructor method
        """
        self.radius_ini = radius_ini
        self.radius_end = radius_end
        self.length = length
        self.num_elements = num_elements
        self.radius_contact = radius_contact
        self.id_contact_up = None
        self.id_contact_down = None

        self.id_ini = None
        self.id_end = None

        self.id_ini_up = None
        self.id_end_up = None

        self.id_ini_down = None
        self.id_end_down = None

    def print(self):
        """Print the variables stored in the Class Section
        """
        print('radius_ini:', self.radius_ini)
        print('radius_end:', self.radius_end)
        print('length:', self.length)
        print('num_elements:', self.num_elements)
        print('radius_contact:', self.radius_contact)
        print('id_contact_up:', self.id_contact_up)
        print('id_contact_down:', self.id_contact_down)
        print('id_ini:', self.id_ini)
        print('id_end:', self.id_end)
        print('id_ini_up:', self.id_ini_up)
        print('id_end_up:', self.id_end_up)
        print('id_ini_down:', self.id_ini_down)
        print('id_end_down:', self.id_end_down)

    def compute_relative_positions(self):
        """ Compute the position in the X axis for each node of the section.\n
            Example: length = 1,  num_elements = 5
                return = [0, 0.2, 0.4, 0.6, 0.8, 1]

            :return: vector with the X values for each node of the section
            :rtype: numpy array
        """
        relative_positions = np.linspace(0, self.length, self.num_elements + 1)
        return relative_positions

    def compute_average_radius(self):
        """ Compute the average radius of the section

        :return: average radius of the section
        :rtype: double
        """
        return (self.radius_ini + self.radius_end) * 0.5

    def compute_volume(self):
        """ Compute the volume of the section

        :return: volume of the section
        :rtype: double
        """
        volume = self.length * math.pi * self.compute_average_radius() ** 2
        return abs(volume)
