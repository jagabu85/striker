import numpy as np

def define_variables():
    """ Defines the input variables to perform the striker simulations, it \
    means define all the Material properties, geometry, and numerical \
    parameters to create the Oofelie script (.e file).

    :return variables: Dictionary with all the variables need to define the \
    geometries of the model (except the striker) and the numerical parameters.
    :type variables: Dictionary
    """
    variables = {
        # Initial geometry configuration
        'apply_initial_rotation': True,
        'move_nodes_y': True,
        'apply_initial_deformation': False, # check function \
        # apply_initial_deformation. It should be edited to apply the desired \
        # initial deformation

        # Hammer simulation to compute the hammer initial conditions
        'hammer_time_step': 1e-4,
        'hammer_num_steps': 81,

        # Striker simulation
        'time_step': 5e-6,
        'final_time': 1e-5*(130), #130

        # Hammer (Represented as one single node with initial velocity
        'hammer_coordinate': [0.000500, 0],  # [mm]
        'hammer_initial_velocity': 0,  # -5.5,  # [m/s]

        # Hammer (Represented as rigid body + spring)
        'hammer_spring_stiffness': 4581.4,  #N/m k = 98.5N/0.0215mm
        'hammer_spring_length': 0.059500,
        # 'hammer_nodes': np.array([[0.015332, -0.022115, 0, 'center_of_mass'],  # FN
        'hammer_nodes': np.array([[0.015332, -0.022115, 0, 'hammer_com'], # com is center of mass #not right
                                [0.003244, -0.032600, 0, 'hammer_hinge'],
                                [0.015332, -0.022115, 0, 'hammer_spring'],
                                [0.024039, -0.007008, 0, 'hammer_impact'],
                                [0.062744, -0.018600, 0, 'hammer_spring_fix']]),

        # Hammer contact
        'hammer_friction': 0,

        # Striker material
        'density': 7800,
        'young_modulus': 210e9,
        'shear_modulus': 79.3e9,
        'poisson_ratio': 0.3,

        # Stopper F up (to stop the recoil)
        'stopper_F_x': -0.019466,  # [mm]
        'stopper_F_y': 0.002866,  # [mm]

        # Stopper G up/down(to stop the strike)
        'stopper_G_x': -0.042396,  # [m]
        'stopper_G_y': 0.003,  # [m]

        # Bullet modeled as spring, spring caracteristiques
        'bullet_stiffness': 1.3e6,  # Setting this stiffness to 0  means there is
                                  # no bullet to contact
        'spring_bullet_length': -0.005,  # I does not have any influence
        'distance_to_bullet': -0.000743,  # distance striker tip to bullet

        #Contact striker guide
        'friction': 0.2,

        # Numerical parameters for the solver
        'max_num_iterations': 100,
        'spectral_radius': 0.6,
        'reference_value': 5e-5,
        'tolerance': 5e-5,
        'displacement_tolerance': 5e-5,

        # Gravity
        'shooting_angle' : np.deg2rad(30),

        # Geometry modifications
        'offset_y': -0.0000025
    }

    return variables

def define_geometry():
    """ Defines the shaft geometry.
    The cap of the columns is:
    initial radius, final radius, length of the section, number of elements, \
    and the last is the contact radius.

    For better understanding check the image in :ref:`intro-label`.
    """

    tmp = np.array([[2,     2,      5.35,      11,  None],
                    [2,     3,      1,          2,  None],
                    [3.5,   3.5,    0.5,        1,  0.0035025],
                    [3.5,   1.5,    4.712,      10, None],
                    [1.5,   1.5,    6.538,      13, None],
                    [1.5,   3.5,    2,          4,  None],
                    [3.5,   3.5,    1,          2,  0.0035025],
                    [3.5,   1.4,    2.1,        4,  None],
                    [1.4,   1.4,    16.15,      33, None],
                    [1.4,   2.5,    1.1,        2,  None],
                    [2.5,   2.5,    0.5,        1,  None],
                    [2.5,   1.4,    1.1,        3,  None],
                    [1.4,   1.4,    34.3,       69, 0.00151],
                    [1.4,   1.25,   0.654,      2,  None],
                    [1.25,  1.25,   10.392,     21, None],
                    [1.25,  1.4,    0.654,      2,  None],
                    [1.4,   1.4,    3.407,      7,  0.00151],
                    [1.4,   0.77,   1.998,      4,  None],
                    [0.77,  0.77,   1.858,      4,  None],
                    [0.77,  0.35,    0.767,      5,  None]
                    ])  # this is defined in milimeter [mm]

    tmp[:, 2] = -tmp[:, 2]  # inverse the direction in the X axis
    tmp[:, 0:3] = tmp[:, 0:3] * 0.001  # The values are converted to meters

    return tmp

def compute_time_discretization_striker_simulation(variables):
    num_steps = int(variables['final_time'] / variables['time_step'])
    final_time_hammer = variables['hammer_time_step'] * \
                        variables['hammer_num_steps']
    final_time = final_time_hammer + variables['final_time']

    time_discretization = np.linspace(final_time_hammer, final_time, num_steps)
    return time_discretization