import os
import numpy as np

def create_string_to_partial_fix_node(id_node, dof_to_fix, comment=""):
    """ Creates a string to be added in the oofelie script. It \
    is used to restrict some of the degrees of fredoms of a node.

    :param id_node: oofelie ID node to be constrained
    :type id_node: int, mandatory
    :param dof_to_fix: Degree of freedom to be fix, it can be: TX, TY, TZ, \
    RX, RY or RZ
    :type dof_to_fix: string, mandatory
    :param comment: String to add a comment in the oofelie file, it is used \
    to easily debug or review the Oofelie script
    :type comment: string, optional
    :return tmp: piece of oofelie script
    :type tmp: string
    """
    tmp = ""
    tmp += "//---------------------------------- " + comment + "\n"
    for dof in dof_to_fix:
        tmp += f'fix.define({id_node}, {dof});\n'

    return tmp


def create_string_to_fix_node(id_node, comment=""):
    """ Creates a string to be added in the oofelie script. It \
    is used to completly fix a one node.

    :param id_node: oofelie ID node to be constrained
    :type id_node: int, mandatory
    :param comment: String to add a comment in the oofelie file, it is used \
    to easily debug or review the Oofelie script
    :type comment: string, optional
    :return tmp: piece of oofelie script
    :type tmp: string
    """
    tmp = create_string_to_partial_fix_node(id_node, ["TX", "TY", "TZ", "RX",
                                                      "RY", "RZ"], comment)
    return tmp


def create_string_contact_stopper_up(_simulation, contact_section, contac_node,
                                     contact_element):
    """ Creates a string to be added in the oofelie script. It \
    is used to create the contact elements of the stopers of the upper part \
    in the X direction.

    :param _simulation: Object simulation, contain the last used IDs in \
    oofelie
    :type _simulation: object simulation,  mandatory
    :param contact_section: Section object to be constrained by the contact
    :type contact_section: Section, mandatory
    :param contac_node: Fixed node, the one attached to the contact surface
    :type contac_node: int, mandatory
    :param contact_element: Element object identifier (it is a name, not a \
    number) and should be previusly defined in the oofelie script.
    :type contact_element: string, mandatory
    :return tmp: piece of oofelie script
    :type tmp: string
    """
    # contact_section is an object of section class
    # contact_node is the id of the contact node
    tmp = ''

    for section_node in range(contact_section.id_ini_up + 1,
                              contact_section.id_end_up + 2):
        tmp += f'elements.define({_simulation.get_next_oofelie_element()}, ' \
                f'{contact_element}, {contac_node}, {section_node});\n'

    return tmp


def create_string_contact_stopper_down(simulation, contact_section,
                                       contac_node, contact_element):
    """ Creates a string to be added in the oofelie script. It \
    is used to create the contact elements of the stopers of the lower \
    part in the X direction.

    :param _simulation: Object simulation, contain the last used IDs in \
    oofelie
    :type _simulation: object simulation,  mandatory
    :param contact_section: Section object to be constrained by the contact
    :type contact_section: Section, mandatory
    :param contac_node: Fixed node, the one attached to the contact surface
    :type contac_node: int, mandatory
    :param contact_element: Element object identifier (it is a name, not a \
    number) and should be previusly defined in the oofelie script.
    :type contact_element: string, mandatory
    :return tmp: piece of oofelie script
    :type tmp: string
    """
    tmp = ''

    for section_node in range(contact_section.id_ini_down + 1,
                              contact_section.id_end_down + 2):
        tmp += f'elements.define({simulation.get_next_oofelie_element()}, ' \
               f'{contact_element}, {contac_node}, {section_node});\n'

    return tmp


def save_e_file(oofelie_script, path, file_name = 'test'):
    """ Creates a string to be added in the oofelie script. It \
    is used to restrict some of the degrees of fredoms of one node.

    :param oofelie_script: oofelie script to be saved, it will be the Oofelie \
    input
    :type oofelie_script: string, mandatory
    :param path: path where the .e should be saved
    :type path: string, mandatory
    :param comment: name of the file where to save the script
    :type comment: string, mandatory
    """
    if not (path):
        os.makedirs(path)
        print("Create results folder")

    print(path + "/" + file_name + ".e")
    text_file = open(path + "/" + file_name + ".e", "w")
    print("Oofelie scrip saved in " + path  + file_name + ".e")

    text_file.write(oofelie_script)
    text_file.close()
    print('--------------------------------------------- Oofelie script saved')

def create_string_hammer_initial_condition(id_hammer_nodes, node_list, path):
    """ Creates a string to be added in the oofelie script. It \
    is used to create the contact elements of the stopers of the lower \
    part in the X direction.

    :param id_hammer_nodes: store the important id nodes, with a meaning name
    :type id_hammer_nodes: dictionary, mandatory
    :param node_list: label to access the dictionary and to read initial \
        conditions from files
    :type node_list: numpy array of strings, mandatory
    :return tmp: piece of oofelie script
    :type tmp: string
    """

    # ic_ means initial condition hammer
    tmp = ''

    for node_label in node_list:
        initial_conditions = np.fromfile(path+ '/' + node_label +'.dat',
                                         dtype=np.double)
        tmp += f"""//---------------------------------------------------
// {node_label}
//----------------------------------------------------
dom.get_properties(TX|GD).put_val_NFD({id_hammer_nodes[node_label]}, TX, {initial_conditions[0]});
dom.get_properties(TY|GD).put_val_NFD({id_hammer_nodes[node_label]}, TY, {initial_conditions[1]});
dom.get_properties(TZ|GD).put_val_NFD({id_hammer_nodes[node_label]}, TZ, {initial_conditions[2]});

dom.get_properties(RX|GD).put_val_NFD({id_hammer_nodes[node_label]}, RX, {initial_conditions[3]});
dom.get_properties(RY|GD).put_val_NFD({id_hammer_nodes[node_label]}, RY, {initial_conditions[4]});
dom.get_properties(RZ|GD).put_val_NFD({id_hammer_nodes[node_label]}, RZ, {initial_conditions[5]});
//---------------------------------------------------
dom.get_properties(TX|GV).put_val_NFD({id_hammer_nodes[node_label]}, TX, {initial_conditions[6]});
dom.get_properties(TY|GV).put_val_NFD({id_hammer_nodes[node_label]}, TY, {initial_conditions[7]});
dom.get_properties(TZ|GV).put_val_NFD({id_hammer_nodes[node_label]}, TZ, {initial_conditions[8]});

dom.get_properties(RX|GV).put_val_NFD({id_hammer_nodes[node_label]}, RX, {initial_conditions[9]});
dom.get_properties(RY|GV).put_val_NFD({id_hammer_nodes[node_label]}, RY, {initial_conditions[10]});
dom.get_properties(RZ|GV).put_val_NFD({id_hammer_nodes[node_label]}, RZ, {initial_conditions[11]});
//---------------------------------------------------
dom.get_properties(TX|GA).put_val_NFD({id_hammer_nodes[node_label]}, TX, {initial_conditions[12]});
dom.get_properties(TY|GA).put_val_NFD({id_hammer_nodes[node_label]}, TY, {initial_conditions[13]});
dom.get_properties(TZ|GA).put_val_NFD({id_hammer_nodes[node_label]}, TZ, {initial_conditions[14]});

dom.get_properties(RX|GA).put_val_NFD({id_hammer_nodes[node_label]}, RX, {initial_conditions[15]});
dom.get_properties(RY|GA).put_val_NFD({id_hammer_nodes[node_label]}, RY, {initial_conditions[16]});
dom.get_properties(RZ|GA).put_val_NFD({id_hammer_nodes[node_label]}, RZ, {initial_conditions[17]});
//---------------------------------------------------\n"""

    return tmp