from . import *
from .simulation import Simulation
from .shaft import Shaft


def create_hammer_e_script(variables):
    """ Creates the oofelie file (e. file) to simulate only the hammer. \
    This is the input file to lunch the hammer simulation in Oofelie.
    It is used to compute fast the hammer initial conditions

    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary
    """

    time_step = variables['hammer_time_step']
    num_steps= variables['hammer_num_steps']
    final_time = time_step * num_steps
    nodes_coordinates = variables['hammer_nodes']
    spring_stiffness = variables['hammer_spring_stiffness']
    spring_length = variables['hammer_spring_length']

    # Create the string of the .e file
    oofelie_script = (
        f"""Domain dom("hammer");
dom.setSpatialHypothesis(THREE_DIM);

double timeStep = {time_step};
double finalTime = {final_time};
        
//!-----------------------!
//! PropElem definitions  !
//!-----------------------!
//-----------------------------------------------------------------------------
double pk = 6.238E-03; // It is the striker mass
//-----------------------------------------------------------------------------
Propelem rigidBodyHammer(RigidBody_new_E);
rigidBodyHammer.put(MASS, 0.0447);
rigidBodyHammer.put(MASS_INERTIA_XX, 1e-6);
rigidBodyHammer.put(MASS_INERTIA_YY, 1e-6);
rigidBodyHammer.put(MASS_INERTIA_ZZ, 1e-5);
rigidBodyHammer.put(PENALTY_SMO, pk/timeStep);
rigidBodyHammer.put(SCALING_SMO, pk/timeStep);
rigidBodyHammer.put(PENALTY_POS, pk);
rigidBodyHammer.put(SCALING_POS, pk);
rigidBodyHammer.put(PENALTY_VEL, pk);
rigidBodyHammer.put(SCALING_VEL, pk);
//-----------------------------------------------------------------------------
Propelem springHammer(SpringDamper_new_E){{
    STIFFNESS,            {spring_stiffness};
    NATURAL_LENGTH        {spring_length};
    DAMPING        0.00000;
}}

//!------------------------!
//! PositionSet definition !
//!------------------------!
print("Define positioSet");
PositionSet posNodes;
posNodes.define(615, {nodes_coordinates[0,0]}, {nodes_coordinates[0,1]}, 0.000000); //center_of_mass
posNodes.define(616, {nodes_coordinates[1,0]}, {nodes_coordinates[1,1]}, 0.000000); //hammer_hinge
posNodes.define(617, {nodes_coordinates[2,0]}, {nodes_coordinates[2,1]}, 0.000000); //hammer_spring
posNodes.define(618, {nodes_coordinates[3,0]}, {nodes_coordinates[3,1]}, 0.000000); //hammer_impact
//---------------------------------------------------------- hammer spring fix
posNodes.define(619, {nodes_coordinates[4,0]}, {nodes_coordinates[4,1]}, 0.000000); // hammer_spring_fix
dom.add(posNodes);
posNodes$

//!-----------------------!
//! ElementSet definition !
//!-----------------------!
print("Define elementSet");
ElementSet elements();
elements.define(570, rigidBodyHammer, 615, 617, 616, 618);
elements.define(571, springHammer, 617, 619);
dom.add(elements);
elements$

//!------------------------!
//! FixationSet definition !
//!------------------------!
FixationSet fix;
//---------------------------------- center of mass node
fix.define(615, TZ);
fix.define(615, RX);
fix.define(615, RY);
//---------------------------------- hammer hinge node
fix.define(616, TX);
fix.define(616, TY);
fix.define(616, TZ);
fix.define(616, RX);
fix.define(616, RY);
//---------------------------------- hammer spring node
fix.define(617, TZ);
fix.define(617, RX);
fix.define(617, RY);
//---------------------------------- hammer impact node
fix.define(618, TZ);
fix.define(618, RX);
fix.define(618, RY);
//---------------------------------- spring hammer fix node
fix.define(619, TX);
fix.define(619, TY);
fix.define(619, TZ);
fix.define(619, RX);
fix.define(619, RY);
fix.define(619, RZ);
dom.add(fix);

//!--------------!
//! Build domain !
//!--------------!
dom.setAnalysis(DYNAMIC_PO);
dom.setStep(1);
dom.build;

//!--------------------------!
//! Solver driver parameters !
//!--------------------------!
NonSmoothGASolver2                  solver (dom);
solver.setFinalTime                 (finalTime);
solver.setTimeStep                  (timeStep);
solver.setTolerance                 (5.00E-05);
solver.setMaximumNumberOfIterations (100);
solver.setSpectralRadiusChungHulbert(5.00E-01); // This is the rho
solver.setReferenceValue            (5.00E-05);
solver.setToleranceDisplacements    (5.00E-05);
solver.setSolveForMultipleCollisions(0);
solver.compute();

//!--------------------------!
//! Save results             !
//!--------------------------!
print("--------------------------------------------------- Save to paraview")
ToParaView toPV(dom);
toPV.exportCodeNode(DISPLACEMENT, "Displacement");
toPV.exportCodeNode(TX|TY|TZ|GV, "Velocity");
toPV.exportCodeNode(TX|TY|TZ|GA, "Accelerations");
toPV.exportCodeNode(TX|TY|TZ|GF, "Forces");
//toPV.exportCodeNode(TX|TY|TZ|GF|I6, "LM");
toPV.writeResults(DYNAMIC_PO, dom.getUserName());

print("---------------------------------- Print and save state of the hammer\n")
int stepToPrint = {num_steps}$
dom.setStep(stepToPrint);
print("## Center of mass - ID: 615 ## \n")
print("---------------------------------------------------------------------\n")
double dis_x = dom[TX|GD][615][1]$
double dis_y = dom[TY|GD][615][2]$
double dis_z = dom[TZ|GD][615][3]$

double dis_rx = dom[RX|GD][615][1]$
double dis_ry = dom[RY|GD][615][2]$
double dis_rz = dom[RZ|GD][615][3]$

double vel_x = dom[TX|GV][615][1]$
double vel_y = dom[TY|GV][615][2]$
double vel_z = dom[TZ|GV][615][3]$

double vel_rx = dom[RX|GV][615][1]$
double vel_ry = dom[RY|GV][615][2]$
double vel_rz = dom[RZ|GV][615][3]$

double acc_x = dom[TX|GA][615][1]$
double acc_y = dom[TY|GA][615][2]$
double acc_z = dom[TZ|GA][615][3]$

double acc_rx = dom[RX|GA][615][1]$
double acc_ry = dom[RY|GA][615][2]$
double acc_rz = dom[RZ|GA][615][3]$

Vector tmpInitialCondition(18);
tmpInitialCondition[1] = dis_x;
tmpInitialCondition[2] = dis_y;
tmpInitialCondition[3] = dis_z;
tmpInitialCondition[4] = dis_rx;
tmpInitialCondition[5] = dis_ry;
tmpInitialCondition[6] = dis_rz;

tmpInitialCondition[7] = vel_x;
tmpInitialCondition[8] = vel_y;
tmpInitialCondition[9] = vel_z;
tmpInitialCondition[10] = vel_rx;
tmpInitialCondition[11] = vel_ry;
tmpInitialCondition[12] = vel_rz;

tmpInitialCondition[13] = acc_x;
tmpInitialCondition[14] = acc_y;
tmpInitialCondition[15] = acc_z;
tmpInitialCondition[16] = acc_rx;
tmpInitialCondition[17] = acc_ry;
tmpInitialCondition[18] = acc_rz;
tmpInitialCondition.saveVector2BinaryFile("hammer_com");

print("## Hammer_hinge - ID: 616 ## \n")
print("---------------------------------------------------------------------\n")
dis_rx = dom[RX|GD][616][1]$
dis_ry = dom[RY|GD][616][2]$
dis_rz = dom[RZ|GD][616][3]$

vel_rx = dom[RX|GV][616][1]$
vel_ry = dom[RY|GV][616][2]$
vel_rz = dom[RZ|GV][616][3]$

acc_rx = dom[RX|GA][616][1]$
acc_ry = dom[RY|GA][616][2]$
acc_rz = dom[RZ|GA][616][3]$

tmpInitialCondition(1) = 0;
tmpInitialCondition(2) = 0;
tmpInitialCondition(3) = 0;
tmpInitialCondition(4) = dis_rx;
tmpInitialCondition(5) = dis_ry;
tmpInitialCondition(6) = dis_rz;

tmpInitialCondition(7) = 0;
tmpInitialCondition(8) = 0;
tmpInitialCondition(9) = 0;
tmpInitialCondition(10) = vel_rx;
tmpInitialCondition(11) = vel_ry;
tmpInitialCondition(12) = vel_rz;

tmpInitialCondition(13) = 0;
tmpInitialCondition(14) = 0;
tmpInitialCondition(15) = 0;
tmpInitialCondition(16) = acc_rx;
tmpInitialCondition(17) = acc_ry;
tmpInitialCondition(18) = acc_rz;
tmpInitialCondition.saveVector2BinaryFile("hammer_hinge");

print("## Hammer_spring -  ID: 617 ##\n")
print("---------------------------------------------------------------------\n")
dis_x = dom[TX|GD][617][1]$
dis_y = dom[TY|GD][617][2]$
dis_z = dom[TZ|GD][617][3]$

dis_rx = dom[RX|GD][617][1]$
dis_ry = dom[RY|GD][617][2]$
dis_rz = dom[RZ|GD][617][3]$

vel_x = dom[TX|GV][617][1]$
vel_y = dom[TY|GV][617][2]$
vel_z = dom[TZ|GV][617][3]$

vel_rx = dom[RX|GV][617][1]$
vel_ry = dom[RY|GV][617][2]$
vel_rz = dom[RZ|GV][617][3]$

acc_x = dom[TX|GA][617][1]$
acc_y = dom[TY|GA][617][2]$
acc_z = dom[TZ|GA][617][3]$

acc_rx = dom[RX|GA][617][1]$
acc_ry = dom[RY|GA][617][2]$
acc_rz = dom[RZ|GA][617][3]$

tmpInitialCondition(1) = dis_x;
tmpInitialCondition(2) = dis_y;
tmpInitialCondition(3) = dis_z;
tmpInitialCondition(4) = dis_rx;
tmpInitialCondition(5) = dis_ry;
tmpInitialCondition(6) = dis_rz;

tmpInitialCondition(7) = vel_x;
tmpInitialCondition(8) = vel_y;
tmpInitialCondition(9) = vel_z;
tmpInitialCondition(10) = vel_rx;
tmpInitialCondition(11) = vel_ry;
tmpInitialCondition(12) = vel_rz;

tmpInitialCondition(13) = acc_x;
tmpInitialCondition(14) = acc_y;
tmpInitialCondition(15) = acc_z;
tmpInitialCondition(16) = acc_rx;
tmpInitialCondition(17) = acc_ry;
tmpInitialCondition(18) = acc_rz;
tmpInitialCondition.saveVector2BinaryFile("hammer_spring");

print("## Hammer_impact - ID: 618 ## \n")
print("---------------------------------------------------------------------\n")
dis_x = dom[TX|GD][618][1]$
dis_y = dom[TY|GD][618][2]$
dis_z = dom[TZ|GD][618][3]$

dis_rx = dom[RX|GD][618][1]$
dis_ry = dom[RY|GD][618][2]$
dis_rz = dom[RZ|GD][618][3]$

vel_x = dom[TX|GV][618][1]$
vel_y = dom[TY|GV][618][2]$
vel_z = dom[TZ|GV][618][3]$

vel_rx = dom[RX|GV][618][1]$
vel_ry = dom[RY|GV][618][2]$
vel_rz = dom[RZ|GV][618][3]$

acc_x = dom[TX|GA][618][1]$
acc_y = dom[TY|GA][618][2]$
acc_z = dom[TZ|GA][618][3]$

acc_rx = dom[RX|GA][618][1]$
acc_ry = dom[RY|GA][618][2]$
acc_rz = dom[RZ|GA][618][3]$

tmpInitialCondition(1) = dis_x;
tmpInitialCondition(2) = dis_y;
tmpInitialCondition(3) = dis_z;
tmpInitialCondition(4) = dis_rx;
tmpInitialCondition(5) = dis_ry;
tmpInitialCondition(6) = dis_rz;

tmpInitialCondition(7) = vel_x;
tmpInitialCondition(8) = vel_y;
tmpInitialCondition(9) = vel_z;
tmpInitialCondition(10) = vel_rx;
tmpInitialCondition(11) = vel_ry;
tmpInitialCondition(12) = vel_rz;

tmpInitialCondition(13) = acc_x;
tmpInitialCondition(14) = acc_y;
tmpInitialCondition(15) = acc_z;
tmpInitialCondition(16) = acc_rx;
tmpInitialCondition(17) = acc_ry;
tmpInitialCondition(18) = acc_rz;
tmpInitialCondition.saveVector2BinaryFile("hammer_impact");

exit;
""")
    print('------------------------------------- Oofelie script hammer defined')
    return oofelie_script
