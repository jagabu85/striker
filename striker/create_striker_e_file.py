from . import *
from .simulation import Simulation
from .shaft import Shaft

def create_striker_e_script(variables, geometry, test=False):
    """ Creates the oofelie file (e. file). This is the input file to lunch \
    the simulation in Oofelie.

    :param variables: dictionary containing all the variables to define \
    the simulation, defined in define_model.py/define_variables()
    :type variables: dictionary
    :param geometry: geometry description to be used for the class shaft
    :type geometry: numpy array, mandatory
    :param test: if FALSE no stoppers added in the simulation, it is just for \
    testing
    :type test: bool, optional
    """
    # Simulation scope
    time_step = variables['time_step']
    final_time = variables['final_time']
    print(f'Number of time steps: {final_time / time_step}')
    print(f'Final time simulation : {final_time:.6f}')

    # Hammer (Represented as one single node with initial velocity
    hammer_coordinate = variables['hammer_coordinate']
    hammer_initial_velocity = variables['hammer_initial_velocity']

    # Hammer (Represented as rigid body + spring)
    #spring_hammer_coordinates = variables['hammer_nodes'][4]
    hammer_spring_stiffness = variables['hammer_spring_stiffness']
    hammer_spring_length = variables['hammer_spring_length']
    hammer_nodes = variables['hammer_nodes']

    # Hammer contact
    hammer_friction = variables['hammer_friction']

    # Striker material
    density = variables['density']
    young_modulus = variables['young_modulus']
    poisson_ratio = variables['poisson_ratio']

    # Stopper F (to stop the recoil)
    stopper_F_x = variables['stopper_F_x']
    stopper_F_y = variables['stopper_F_y']

    # Stopper G (to stop the strike)
    stopper_G_x = variables['stopper_G_x']
    stopper_G_y = variables['stopper_G_y']

    # Bullet modeled as spring, spring characteristiques
    bullet_stiffness = variables['bullet_stiffness']
    spring_bullet_length = variables['spring_bullet_length']
    distance_to_bullet = variables['distance_to_bullet']

    # Contact striker guide
    friction = variables['friction']

    # Numerical parameters for the solver
    max_num_iterations = variables['max_num_iterations']
    spectral_radius = variables['spectral_radius']
    reference_value = variables['reference_value']
    tolerance = variables['tolerance']
    displacement_tolerance = variables['displacement_tolerance']
    shooting_angle = variables['shooting_angle']

    # Creates data for the .e file
    simulation = Simulation()
    striker = Shaft(geometry)
    if variables['apply_initial_rotation'] == True:
        striker.apply_initial_rotation()
    if variables['move_nodes_y'] == True:
        striker.move_nodes_y(variables['offset_y'])
    if variables['apply_initial_deformation'] == True:
        striker.apply_initial_deformation()

    # Create the string of the .e file
    oofelie_script = (
        f"""Domain dom("striker");
dom.setSpatialHypothesis(THREE_DIM);

MemoryManager MM;
MM.setStatusOn();
MM.setNumberOfStepsToKeep(DYNAMIC_PO,3);
MM.setDeltaStepsToSave(DYNAMIC_PO,1);
MM.setNumberOfBackStepsToKeep(DYNAMIC_PO,2);
MM.setQuietModeOff();
        
double timeStep = {time_step:.2E};
double finalTime = {final_time:.2E};
        
double youngModulus = {young_modulus:.2E};
double poissonRatio = {poisson_ratio};
double density = {density};

//!------------------------!
//! MaterialSet definition !
//!------------------------!
MaterialSet materSet;
Material materialBeam (IsoElastMaterial);
materialBeam.put (ELASTIC_MODULUS, youngModulus);
materialBeam.put (POISSON_RATIO,   poissonRatio);
materialBeam.put (MASS_DENSITY,    density) ;
materSet.put (1,materialBeam);
dom.add(materSet);
        
//!-----------------------!
//! PropElem definitions  !
//!-----------------------!
//-----------------------------------------------------------------------------
{striker.create_string_elements_beam_definition()}
double pk = {striker.compute_volume() * density:.3E}$ // It is the striker mass
//-----------------------------------------------------------------------------
Propelem rigidBodyHammer(RigidBody_new_E);
rigidBodyHammer.put(MASS, 0.0447);
rigidBodyHammer.put(MASS_INERTIA_XX, 1e-6);
rigidBodyHammer.put(MASS_INERTIA_YY, 1e-6);
rigidBodyHammer.put(MASS_INERTIA_ZZ, 1e-5);
rigidBodyHammer.put(PENALTY_SMO, pk/(timeStep*1));
rigidBodyHammer.put(SCALING_SMO, pk/(timeStep*1));
rigidBodyHammer.put(PENALTY_POS, pk);
rigidBodyHammer.put(SCALING_POS, pk);
rigidBodyHammer.put(PENALTY_VEL, pk);
rigidBodyHammer.put(SCALING_VEL, pk);
//-----------------------------------------------------------------------------
Propelem rigidBodyMassLess(RigidBody_new_E);
rigidBodyMassLess.put(MASS, 0);
rigidBodyMassLess.put(MASS_INERTIA_XX, 0);
rigidBodyMassLess.put(MASS_INERTIA_YY, 0);
rigidBodyMassLess.put(MASS_INERTIA_ZZ, 0);
rigidBodyMassLess.put(PENALTY_SMO, pk/(timeStep*1));
rigidBodyMassLess.put(SCALING_SMO, pk/(timeStep*1));
rigidBodyMassLess.put(PENALTY_POS, pk);
rigidBodyMassLess.put(SCALING_POS, pk);
rigidBodyMassLess.put(PENALTY_VEL, pk);
rigidBodyMassLess.put(SCALING_VEL, pk);
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
Propelem eleContactHammer(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  -0.665;
    V1Y,  0.747;
    V1Z,  0;
    V2X,  -0.747;
    V2Y,  -0.665;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,  1;
    FRICTION, {hammer_friction};
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   0.0;
    RESTITUTION_TANGENT,  0.0
}}
//-----------------------------------------------------------------------------
Propelem eleContactHammerOneNode(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  -1;
    V1Y,  0;
    V1Z,  0;
    V2X,  0;
    V2Y,  -1;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,  1;
    FRICTION, {hammer_friction};
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   0.0;
    RESTITUTION_TANGENT,  0.0
}}
//-----------------------------------------------------------------------------
Propelem eleContactFloor(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  0;
    V1Y,  1;
    V1Z,  0;
    V2X,  1;
    V2Y,  0;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,  -1;
    FRICTION, {friction};
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   1;
    RESTITUTION_TANGENT,  0.0;
}}
//-----------------------------------------------------------------------------
Propelem eleContactCeiling(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  0;
    V1Y,  -1;
    V1Z,  0;
    V2X,  1;
    V2Y,  0;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,  1;
    FRICTION, {friction};
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   1;
    RESTITUTION_TANGENT,  0.0;
}}
//-----------------------------------------------------------------------------
Propelem eleContactBullet(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  1;
    V1Y,  0;
    V1Z,  0;
    V2X,  0;
    V2Y,  1;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,   1;
    FRICTION, 0;
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   0.0;
    RESTITUTION_TANGENT,  0.0;
}}
//-----------------------------------------------------------------------------
Propelem eleContactStopperF(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  -1;
    V1Y,  -1;
    V1Z,  0;
    V2X,  -1;
    V2Y,  1;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,  -1;
    FRICTION, 0;
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   0.0;
    RESTITUTION_TANGENT,  0.0;
}}
//-----------------------------------------------------------------------------
Propelem eleContactStopperGUp(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  1;
    V1Y,  -1;
    V1Z,  0;
    V2X,  1;
    V2Y,  1;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,  1;
    FRICTION, 0;
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   0.0;
    RESTITUTION_TANGENT,  0.0;
}}
//-----------------------------------------------------------------------------
Propelem eleContactStopperGDown(ContactNodeToFaceWithFricctionAndRotation_E){{
    V1X,  1;
    V1Y,  1;
    V1Z,  0;
    V2X,  1;
    V2Y,  -1;
    V2Z,  0;
    V3X,  0;
    V3Y,  0;
    V3Z,  -1;
    FRICTION, 0;
    SCALING_LAGRANGE_MULTIPLIER,   1;
    PENALTY_NORMAL_POSITION,       pk;
    PENALTY_TANGENT_POSITION,      pk;
    PENALTY_NORMAL_VELOCITY,       pk;
    PENALTY_TANGENT_VELOCITY,      pk;
    RESTITUTION_NORMAL,   0.0;
    RESTITUTION_TANGENT,  0.0;
}}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
Propelem springBullet(SpringDamper_new_E){{
    STIFFNESS,            {bullet_stiffness:.4E};
}}
//-----------------------------------------------------------------------------
Propelem springHammer(SpringDamper_new_E){{
    STIFFNESS,            {hammer_spring_stiffness:.6f};
    NATURAL_LENGTH        {hammer_spring_length:.6f};
    DAMPING        0.00000;
}}

//!------------------------!
//! PositionSet definition !
//!------------------------!
print("Define positioSet");
PositionSet posNodes;
//------------------------------------------striker Nodes (beams + rigidbodies)
{striker.create_string_beam_nodes(simulation)}
//----------------------------------------------------------upper contact nodes
{striker.create_string_contact_nodes_up(simulation)}
//----------------------------------------------------------lower contact nodes
{striker.create_string_contact_nodes_down(simulation)}
//-------------------------------------------------- spring bullet contact node
posNodes.define({simulation.get_next_oofelie_node('bullet_contact')}, {striker.nodes[-1, 0] + distance_to_bullet:.6f},  0.000000, 0.000000);
posNodes.define({simulation.get_next_oofelie_node('bullet_fix')}, {striker.nodes[-1, 0] + distance_to_bullet + spring_bullet_length:.6f},  0.000000, 0.000000);
//------------------------------------------------------------- hammer one node
posNodes.define({simulation.get_next_oofelie_node('hammer_impact_one_node')}, {hammer_coordinate[0]:.6f},  {hammer_coordinate[1]:.6f}, 0.000000);
//---------------------------------------------------------------- hammer nodes
posNodes.define({simulation.get_next_oofelie_node(hammer_nodes[0, 3])},  {hammer_nodes[0, 0]}, {hammer_nodes[0, 1]}, 0.000000); //hamer_com com = center of mass
posNodes.define({simulation.get_next_oofelie_node(hammer_nodes[1, 3])},  {hammer_nodes[1, 0]}, {hammer_nodes[1, 1]}, 0.000000); //hammer_hinge
posNodes.define({simulation.get_next_oofelie_node(hammer_nodes[2, 3])},  {hammer_nodes[2, 0]}, {hammer_nodes[2, 1]}, 0.000000); //hammer_spring
posNodes.define({simulation.get_next_oofelie_node(hammer_nodes[3, 3])},  {hammer_nodes[3, 0]}, {hammer_nodes[3, 1]}, 0.000000); //hammer_impact
//---------------------------------------------------- hammer spring fixed node
posNodes.define({simulation.get_next_oofelie_node(hammer_nodes[4, 3])}, {hammer_nodes[4, 0]}, {hammer_nodes[4, 1]}, 0.000000); //hammer_spring_fix
//----------------------------------------------------------- stopper contact F
posNodes.define({simulation.get_next_oofelie_node('stopper_F')}, {stopper_F_x:.6f}, {stopper_F_y:.6f}, 0.000000);
//----------------------------------------------------------- stopper contact G
posNodes.define({simulation.get_next_oofelie_node('stopper_G_up')}, {stopper_G_x:.6f}, {stopper_G_y:.6f}, 0.000000);
posNodes.define({simulation.get_next_oofelie_node('stopper_G_down')}, {stopper_G_x:.6f}, {-stopper_G_y:.6f}, 0.000000);                                  
dom.add(posNodes);
posNodes$

//!-----------------------!
//! ElementSet definition !
//!-----------------------!
print("Define elementSet");
ElementSet elements();
//------------------------------------------------------- striker beam elements
{striker.create_string_beam_elements_striker(simulation)}
//----------------------------------------------- striker rigid bodies elememts
{striker.create_string_rigid_elements_striker(simulation)}
//------------------------------------------------------ upper contact elememts
{striker.create_string_contact_elements_up(simulation)}
//------------------------------------------------------ lower contact elememts
{striker.create_string_contact_elements_down(simulation)}
//----------------------------------------------------- hammer one node elememt
elements.define({simulation.get_next_oofelie_element()}, rigidBodyHammer, {simulation.id_nodes['hammer_impact_one_node']});
elements.define({simulation.get_next_oofelie_element()}, eleContactHammerOneNode, {simulation.id_nodes['hammer_impact_one_node']}, 1);
//------------------------------------------------------------- hammer elememts
elements.define({simulation.get_next_oofelie_element()}, rigidBodyHammer, {simulation.id_nodes['hammer_com']}, {simulation.id_nodes['hammer_spring']}, {simulation.id_nodes['hammer_hinge']}, {simulation.id_nodes['hammer_impact']});
elements.define({simulation.get_next_oofelie_element()}, springHammer, {simulation.id_nodes['hammer_spring']}, {simulation.id_nodes['hammer_spring_fix']});
elements.define({simulation.get_next_oofelie_element()}, eleContactHammer, {simulation.id_nodes['hammer_impact']}, 1);
//---------------------------------- bullet contact element
elements.define({simulation.get_next_oofelie_element()}, eleContactBullet, {simulation.id_nodes['bullet_contact']}, {striker.get_num_nodes_beam()});
//---------------------------------- bullet spring element
elements.define({simulation.get_next_oofelie_element()}, springBullet, {simulation.id_nodes['bullet_contact']}, {simulation.id_nodes['bullet_fix']});
""")

    if test is False:
        oofelie_script += (
            f"""//---------------------------------------------------- stoper F contact element
{create_string_contact_stopper_up(simulation, striker.section_list[5], simulation.id_nodes['stopper_F'], 'eleContactStopperF')[:-1]}
//--------------------------------------------------- stoper G contact elements
{create_string_contact_stopper_up(simulation, striker.section_list[11], simulation.id_nodes['stopper_G_up'], 'eleContactStopperGUp')[:-1]}
{create_string_contact_stopper_down(simulation, striker.section_list[11], simulation.id_nodes['stopper_G_down'], 'eleContactStopperGDown')[:-1]}
""")


    oofelie_script += (
        f"""dom.add(elements);
elements$

//!------------------------!
//! FixationSet definition !
//!------------------------!
FixationSet fix;
//-------------------------------------------- striker beam + rigidBodies nodes
{striker.create_string_fix_beam_nodes()}
//--------------------------------------------------------- guide contact nodes
{striker.create_string_fix_contact_nodes()}
//------------------------------------------------- bullet contact and fix node
{create_string_to_partial_fix_node(simulation.id_nodes['bullet_contact'],
                                   ["TY", "TZ", "RX", "RY", "RZ"],
                                   "bullet contact node")[:-1]}
{create_string_to_fix_node(simulation.id_nodes['bullet_fix'],
                           "fix bullet contact node")[:-1]}
//------------------------------------------------------------- hammer one node
{create_string_to_partial_fix_node(simulation.id_nodes['hammer_impact_one_node'],
                                   ["TY", "TZ", "RX", "RY", "RZ"],
                                   "hammer one node")[:-1]}
//---------------------------------------------------------------------- hammer
{create_string_to_partial_fix_node(simulation.id_nodes[hammer_nodes[0, 3]],
                                   ["TZ", "RX", "RY"],
                                   "center of mass node")[:-1]}
{create_string_to_partial_fix_node(simulation.id_nodes[hammer_nodes[1, 3]],
                                   ["TX", "TY", "TZ", "RX", "RY"],
                                   "hammer hinge node")[:-1]}
{create_string_to_partial_fix_node(simulation.id_nodes[hammer_nodes[2, 3]],
                                   ["TZ", "RX", "RY"],
                                   "hammer spring node")[:-1]}
{create_string_to_partial_fix_node(simulation.id_nodes[hammer_nodes[3, 3]],
                                   ["TZ", "RX", "RY"],
                                   "hammer impact node")[:-1]}
//------------------------------------------------------------fix spring hammer
{create_string_to_fix_node(simulation.id_nodes['hammer_spring_fix'],
                           "spring hammer fix node")[:-1]}
//-------------------------------------------------------------------- stoppers
{create_string_to_fix_node(simulation.id_nodes['stopper_F'],
                           "fix contact node stopper F")[:-1]}
{create_string_to_fix_node(simulation.id_nodes['stopper_G_up'],
                           "fix contact node stopper G up")[:-1]}
{create_string_to_fix_node(simulation.id_nodes['stopper_G_down'],
                           "fix contact node stopper G down")[:-1]}
dom.add(fix);

//!-----------!
//! Add loads !
//!-----------!
ExcitationSet excSet;
{striker.create_string_gravity_load(density, shooting_angle)}
dom.dom_add(excSet);
excSet$

dom.setAnalysis(DYNAMIC_PO);
dom.setStep(1);
dom.get_properties(TX|GV).put_val_NFD({simulation.id_nodes['hammer_impact_one_node']}, TX, {hammer_initial_velocity});

{create_string_hammer_initial_condition(simulation.id_nodes, 
                                        np.array(['hammer_com', 
                                                  'hammer_hinge', 
                                                  'hammer_spring',
                                                  'hammer_impact']),
                                        variables['path'])}
dom.build;

//!--------------------------!
//! Solver driver parameters !
//!--------------------------!
NonSmoothGASolver2                  solver (dom);
solver.setFinalTime                 (finalTime);
solver.setTimeStep                  (timeStep);
solver.setTolerance                 ({tolerance:.2E});
solver.setMaximumNumberOfIterations ({max_num_iterations});
solver.setSpectralRadiusChungHulbert({spectral_radius:.2E}); // This is the rho
solver.setReferenceValue            ({reference_value:.2E});
solver.setToleranceDisplacements    ({displacement_tolerance:.2E});
solver.setSolveForMultipleCollisions(0);
solver.compute();

//!--------------------------!
//! Save results             !
//!--------------------------!
print("--------------------------------------------------- Save to paraview\n")
ToParaView toPV(dom);
toPV.exportCodeNode(DISPLACEMENT, "Displacement");
toPV.exportCodeNode(TX|TY|TZ|GV, "Velocity");
toPV.exportCodeNode(TX|TY|TZ|GA, "Accelerations");
toPV.exportCodeNode(TX|TY|TZ|GF, "Forces");
//toPV.exportCodeNode(TX|TY|TZ|GF|I6, "LM");
toPV.writeResults(DYNAMIC_PO, dom.getUserName());
print("--------------------------------------------------- Save main values\n")
Vector tmpVectorOneValue(1) = {time_step};
tmpVectorOneValue.saveVector2BinaryFile("timeStep");

int numSteps = dom.getNumberOfSteps();
tmpVectorOneValue(1) = numSteps;
tmpVectorOneValue.saveVector2BinaryFile("numSteps");

int numElements = {striker.num_elements};
tmpVectorOneValue(1) = numElements;
tmpVectorOneValue.saveVector2BinaryFile("numElements");

print("------------------------------------------------------- Save strains\n")
Vector tmpVector (numSteps);

Vector gamma_1(numSteps*numElements);
Vector gamma_2(numSteps*numElements);
Vector gamma_3(numSteps*numElements);
Vector kappa_1(numSteps*numElements);
Vector kappa_2(numSteps*numElements);
Vector kappa_3(numSteps*numElements);

int j;
int step; 
for (step = 1; step <= numSteps; step++)
{{ 
    dom.setStep(step); // <- Here is the fucking problem
    Reference values_gamma(dom.getDBEntry(STRAIN_GAMMA_DBE));
    Reference values_kappa(dom.getDBEntry(STRAIN_KAPPA_DBE));

    for(j=1;j<=values_gamma.getNbEntities();j++)
    {{
        Vect3 gamma=values_gamma.getVect3(j);
        Vect3 kappa=values_kappa.getVect3(j);

        gamma_1[(numElements*(step-1))+j] = gamma[1];
        gamma_2[(numElements*(step-1))+j] = gamma[2];
        gamma_3[(numElements*(step-1))+j] = gamma[3];

        kappa_1[(numElements*(step-1))+j] = kappa[1];
        kappa_2[(numElements*(step-1))+j] = kappa[2];
        kappa_3[(numElements*(step-1))+j] = kappa[3];
    }};
step$
}};

gamma_1.saveVector2BinaryFile("gamma_1");
gamma_2.saveVector2BinaryFile("gamma_2");
gamma_3.saveVector2BinaryFile("gamma_3");

kappa_1.saveVector2BinaryFile("kappa_1");
kappa_2.saveVector2BinaryFile("kappa_2");
kappa_3.saveVector2BinaryFile("kappa_3");

print("---------------------------------------------------- Save velocities\n")
Function scalar getDofTimeResponseBeamStrikerNodes(Vector response, Domain D, Lock lock, scalar componenent, scalar numNodesToExtract)
{{
    Vector tmpVectorNodes (numNodesToExtract);
    int FnumSteps = D.getNumberOfSteps()$
    int TimeStep;
    for (TimeStep = 1; TimeStep <= FnumSteps; TimeStep++)
    {{
        D.setStep(TimeStep); // -> this one is givin an error, but not always
        TimeStep$
        int activeNode = 0; // Node
        for ( activeNode = 1; activeNode<=numNodesToExtract; activeNode++ )
        {{
            D[lock].define(activeNode);
            tmpVectorNodes[activeNode] = D[lock][activeNode][componenent];
        }};
        response.fill_with(tmpVectorNodes,1+(TimeStep-1)*numNodesToExtract); 
    }};
    return 0;
}};
print("------------------------------------- After function save velocities\n")
int numNodes = {striker.get_num_nodes_beam()}$
Vector velocitiesX(numNodes*numSteps);
getDofTimeResponseBeamStrikerNodes(velocitiesX, dom, TX|GV, 1, numNodes);
velocitiesX.saveVector2BinaryFile("velocities_x");

Vector velocitiesY(numNodes*numSteps);
getDofTimeResponseBeamStrikerNodes(velocitiesY, dom, TY|GV, 2, numNodes);
velocitiesY.saveVector2BinaryFile("velocities_y");

Vector displacementsX(numNodes*numSteps);
getDofTimeResponseBeamStrikerNodes(displacementsX, dom, TX|GD, 1, numNodes);
displacementsX.saveVector2BinaryFile("displacements_x");

Vector displacementsY(numNodes*numSteps);
getDofTimeResponseBeamStrikerNodes(displacementsY, dom, TY|GD, 2, numNodes);
displacementsY.saveVector2BinaryFile("displacements_y");
exit;
""")
    print('------------------------------------------- Oofelie script defined')
    return oofelie_script, striker
